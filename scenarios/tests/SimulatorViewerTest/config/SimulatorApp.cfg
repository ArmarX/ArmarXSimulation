# ==================================================================
# ArmarX properties
# ==================================================================

# ArmarX.CachePath:  Path for cache files
#  Attributes:
#  - Default:            ${HOME}/.armarx/mongo/.cache
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.CachePath = ${HOME}/.armarx/mongo/.cache


# ArmarX.DataPath:  Semicolon-separated search list for data files
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.DataPath = ""


# ArmarX.Verbosity:  Global logging level for whole application
#  Attributes:
#  - Default:            Verbose
#  - Case sensitivity:   no
#  - Required:           no
#  - Possible values: {Debug, Error, Fatal, Important, Info, Undefined, Verbose, Warning}
#ArmarX.Verbosity = Debug


# ArmarX.DisableLogging:  Turn logging off in whole application
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.DisableLogging = 0


# ArmarX.ApplicationName:  Application name
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.ApplicationName = ""


# ArmarX.DependenciesConfig:  Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.
#  Attributes:
#  - Default:            ./config/dependencies.cfg
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.DependenciesConfig = ./config/dependencies.cfg


# ArmarX.Config:  Comma-separated list of configuration files 
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Config = ""


# ==================================================================
# ArmarX.Simulator properties
# ==================================================================

# ArmarX.Simulator.MinimumLoggingLevel:  Local logging level only for this component
#  Attributes:
#  - Default:            Undefined
#  - Case sensitivity:   no
#  - Required:           no
#  - Possible values: {Error, Fatal, Info, Undefined, Verbose, Warning}
# ArmarX.Simulator.MinimumLoggingLevel = Undefined

# ArmarX.Simulator.ObjectName:  Name of IceGrid well-known object
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.ObjectName = ""


### Setup

# ArmarX.Simulator.FixedTimeStepStepTimeMS:  The simulation's internal timestep (fixed time step mode)
#  Attributes:
#  - Default:            2
#  - Case sensitivity:   no
#  - Required:           no
ArmarX.Simulator.FixedTimeStepStepTimeMS = 1

# ArmarX.Simulator.FixedTimeStepLoopNrSteps:  The number of internal simulation loops (fixed time step mode)
#  Attributes:
#  - Default:            3
#  - Case sensitivity:   no
#  - Required:           no
ArmarX.Simulator.FixedTimeStepLoopNrSteps = 2

# ArmarX.Simulator.SceneFileName:  Simox/VirtualRobot scene file name, e.g. myScene.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
ArmarX.Simulator.SceneFileName = "ArmarXSimulation/environment/KitchenSim/KIT_Kitchen_Scene.xml"

# ArmarX.Simulator.FloorTexture:  Texture file for floor.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.FloorTexture = ""

# ArmarX.Simulator.FloorPlane:  Enable floor plane.
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.FloorPlane = 1


### Robot

# ArmarX.Simulator.RobotFileName:  Simox/VirtualRobot robot file name, e.g. robot_model.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.RobotFileName = ""

# ArmarX.Simulator.LogRobot:  Enable robot logging. If true, the complete robot state is logged to a file each step.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.LogRobot = 0

# ArmarX.Simulator.RobotControllerPID.p:  Setup robot controllers: PID paramter p.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.p = 10

# ArmarX.Simulator.RobotControllerPID.i:  Setup robot controllers: PID paramter i.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.i = 0

# ArmarX.Simulator.RobotControllerPID.d:  Setup robot controllers: PID paramter d.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.d = 0

# ArmarX.Simulator.InitialRobotPose.x:  x component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.x = 0

# ArmarX.Simulator.InitialRobotPose.y:  y component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.y = 0

# ArmarX.Simulator.InitialRobotPose.z:  z component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.z = 0

# ArmarX.Simulator.InitialRobotPose.roll:  Initial robot pose: roll component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.roll = 0

# ArmarX.Simulator.InitialRobotPose.pitch:  Initial robot pose: pitch component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.pitch = 0

# ArmarX.Simulator.InitialRobotPose.yaw:  Initial robot pose: yaw component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.yaw = 0




### Memory

# ArmarX.Simulator.CommonStorageName:  Name of CommonStorage
#  Attributes:
#  - Default:            CommonStorage
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.CommonStorageName = CommonStorage

# ArmarX.Simulator.PriorKnowledgeName:  Name of PriorKnowledge
#  Attributes:
#  - Default:            PriorKnowledge
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.PriorKnowledgeName = PriorKnowledge

# ArmarX.Simulator.LongtermMemoryName:  Name of LongtermMemory
#  Attributes:
#  - Default:            LongtermMemory
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.LongtermMemoryName = LongtermMemory

# ArmarX.Simulator.WorkingMemoryName:  Name of WorkingMemory
#  Attributes:
#  - Default:            WorkingMemory
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.WorkingMemoryName = WorkingMemory

# ArmarX.Simulator.LongtermMemory.SnapshotName:  Name of snapshot (=MongoDB collection) to load objects from
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   no
#  - Required:           no
# ArmarX.Simulator.LongtermMemory.SnapshotName = ""











