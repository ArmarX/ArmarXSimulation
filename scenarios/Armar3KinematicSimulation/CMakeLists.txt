


set(SCENARIO_CONFIG_COMPONENTS

	config/DebugObserver.cfg
	config/ConditionHandler.cfg
	config/SystemObserver.cfg

	config/CommonStorage.cfg
	config/PriorKnowledge.cfg
	config/LongtermMemory.cfg
	config/WorkingMemory.cfg

	config/RobotStateComponent.cfg
	config/RobotIK.cfg
	config/AStarPathPlanner.cfg
	config/ViewSelectionApp.cfg
    config/ObjectLocalizationSaliencyApp.cfg

	config/SimulatorApp.cfg
	config/SimulatorViewerApp.cfg

    config/InertialMeasurementUnitObserverApp.cfg
    config/IMUSimulationApp.cfg

	config/KinematicUnitDynamicSimulationApp.cfg
	config/KinematicUnitObserver.cfg
	config/PlatformUnitDynamicSimulationApp.cfg
	config/PlatformUnitObserver.cfg
	config/ImageProviderDynamicSimulationApp.cfg
	config/DepthImageProviderDynamicSimulationApp.cfg
	config/ObjectLocalizationDynamicSimulationApp.cfg
	config/ObjectLocalizationDynamicSimulationApp.SegmentableRecognition.cfg
	config/ObjectMemoryObserver.cfg
	config/HandUnitDynamicSimulationApp.LeftHand.cfg
	config/HandUnitDynamicSimulationApp.RightHand.cfg
	config/ForceTorqueUnitDynamicSimulationApp.cfg
	config/ForceTorqueObserver.cfg
	config/SelfLocalizationDynamicSimulationApp.cfg
	config/RobotHandLocalizationDynamicSimulationApp.cfg
	config/HeadIKUnit.cfg
	config/TCPControlUnit.cfg
	config/ProfilerObserverApp.cfg
	config/GraphNodePoseResolverApp.cfg
)


# optional:
#	config/ArmarXGui.cfg


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("Armar3KinemticSimulation" "${SCENARIO_CONFIG_COMPONENTS}" "config/global.cfg")

