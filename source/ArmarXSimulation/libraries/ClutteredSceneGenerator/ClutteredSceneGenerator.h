/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::ClutteredSceneGenerator
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <ArmarXSimulation/libraries/ClutteredSceneGenerator/objects/SimulatedObject.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <VirtualRobot/VirtualRobot.h>

#include <boost/random/mersenne_twister.hpp>

#include <memory>

namespace armarx::simulation::scene_generation
{
    /**
    * @defgroup Library-ClutteredSceneGenerator ClutteredSceneGenerator
    * @ingroup ArmarXSimulation
    * A description of the library ClutteredSceneGenerator.
    *
    * @class ClutteredSceneGenerator
    * @ingroup Library-ClutteredSceneGenerator
    * @brief Brief description of class ClutteredSceneGenerator.
    *
    * Detailed description of class ClutteredSceneGenerator.
    */
    class ClutteredSceneGenerator
    {
    public:
        struct Config
        {
            // Object sets
            std::vector<ObjectSet> objectSets;

            /** Number of objects to generate in a scene */
            uint amountObjects = 6;

            /** Number of simulation steps executed for letting objects fall onto the table */
            uint fallingSteps = 100;

            /** Whether to automatically reset invalid objects after scene generation */
            bool autoResetInvalidObjects = true;
            /** Maximum number of retries when resetting objects */
            uint maxResetTries = 1;

            // Table position and dimensions
            float boxX = 0;
            float boxY = 0;
            float boxZ = 0;
            float boxW = 1500;
            float boxH = 750;
            float boxD = 1000; // approximately

            // Minimum distance between object pivot and table edge
            float objectMarginSide = 200;
            // Initial vertical distance between objects
            float objectSpacingZ = 100;

            // Bounding box for creating objects
            float minObjectX = -550;        // BOX_X - BOX_W / 2 + MARGIN
            float minObjectY = -175;        // BOX_Y - BOX_H / 2 + MARGIN
            float maxObjectX = 550;         // BOX_X + BOX_W / 2 - MARGIN
            float maxObjectY = 175;         // BOX_Y + BOX_H / 2 - MARGIN
            float minObjectZ = 150;

            // If an object has a Z position less than this it will be reset
            float minObjectZOtherwiseReset = 0;
        };

        ClutteredSceneGenerator(const armarx::SimulatorInterfacePrx& simulator,
                                const Config& config);

        void generateScene(int seed);

        [[nodiscard]] const Config &getConfig() const;
        void setConfig(const Config &conf);

        [[nodiscard]] const SimulatorInterfacePrx &getSimulator() const;
        void setSimulator(const SimulatorInterfacePrx &simulator);

    private:
        void createCollisionWalls();

        /**
         * @brief randomObjectPose
         *      Generates a random pose for an object.
         * @param heightIndex
         *      How far above the box the pose should be (z = MIN_OBJECT_Z + heightIndex * OBJECT_SPACING_Z)
         * @return
         *      a random pose.
         */
        armarx::PosePtr randomObjectPose(uint heightIndex);

        /**
         * @brief deleteLocalObjectCopies
         *      Deletes all local copies of the objects in the scene
         */
        void deleteLocalObjectCopies();
        
        static std::unique_ptr<SimulatedObject> makeObject(const std::string& name,
                                                    const ObjectSource& objectSource);

        /**
         * @brief updateLocalObjectCopies
         *      Updates local copies of objects by retrieving their poses from the simulator.
         */
        void updateLocalObjectCopies();

        void dropObjects();
        void resetInvalidObjects();

        Config config_;
        armarx::SimulatorInterfacePrx simulator_;

        boost::mt19937 rnd_;

        std::vector<std::unique_ptr<SimulatedObject>> localObjectCopies_;

        // Walls for collision detection to ensure objects are inside the box
        VirtualRobot::SceneObjectPtr frontWall_;
        VirtualRobot::SceneObjectPtr backWall_;
        VirtualRobot::SceneObjectPtr leftWall_;
        VirtualRobot::SceneObjectPtr rightWall_;
        VirtualRobot::SceneObjectSetPtr walls_;
    };
}  // namespace armarx::simulation::scene_generation
