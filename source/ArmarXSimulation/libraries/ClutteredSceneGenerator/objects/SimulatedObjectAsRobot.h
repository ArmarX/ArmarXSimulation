/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::simulation::scene_generation
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "SimulatedObject.h"

namespace armarx::simulation::scene_generation
{
    class SimulatedObjectAsRobot :
        public SimulatedObject
    {
        public:
            SimulatedObjectAsRobot(const std::string& instanceName, const ObjectSource& source);

            void addToSimulator(armarx::SimulatorInterfacePrx& simulator) override;
            void updatePoseFromSimulator(armarx::SimulatorInterfacePrx& simulator) override;
            void updatePoseToSimulator(armarx::SimulatorInterfacePrx& simulator) override;

            bool checkCollision(const VirtualRobot::CollisionCheckerPtr& col,
                                const VirtualRobot::SceneObjectSetPtr& objectSet) override;
            
            std::unique_ptr<Pose> getLocalPose() override;
            void setLocalPose(const armarx::PosePtr& pose) override;

            ~SimulatedObjectAsRobot() override = default;

        private:
            VirtualRobot::RobotPtr localCopy_;
    };
}  // namespace armarx::simulation::scene_generation
