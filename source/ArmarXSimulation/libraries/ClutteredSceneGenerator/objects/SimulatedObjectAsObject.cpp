/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::simulation::scene_generation
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimulatedObjectAsObject.h"

#include <ArmarXSimulation/libraries/ClutteredSceneGenerator/objects/SimulatedObject.h>

#include "ArmarXCore/core/PackagePath.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/interface/core/PackagePath.h>

#include <VirtualRobot/XML/ObjectIO.h>

namespace armarx::simulation::scene_generation
{
    SimulatedObjectAsObject::SimulatedObjectAsObject(const std::string& instanceName, const ObjectSource& objectSource) :
        SimulatedObject(instanceName, objectSource)
    {
        // TODO(patrick.hegemann): refactor this dirty fix of stupid behavior
        const armarx::data::PackagePath& pp = objectSource.path.serialize();
        std::filesystem::path relPath(pp.path);
        ARMARX_CHECK(!relPath.empty()) << "Relative path to object should not be empty";
        if (!relPath.empty() && relPath.begin()->string() == pp.package)
        {
            relPath = relPath.lexically_relative(*relPath.begin());
        }
        armarx::PackagePath newPackagePath(pp.package, relPath);

        localCopy_ = VirtualRobot::ObjectIO::loadManipulationObject(newPackagePath.toSystemPath());
        localCopy_->setName(instanceName);
    }

    void SimulatedObjectAsObject::addToSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        PosePtr pose(new Pose(localCopy_->getGlobalPose()));
        simulator->addObjectFromFile(getObjectSource().path.serialize(), getInstanceName(), pose, false);
    }

    void SimulatedObjectAsObject::updatePoseFromSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        const PoseBasePtr& poseBase = simulator->getObjectPose(getInstanceName());
        Pose p(poseBase->position, poseBase->orientation);
        localCopy_->setGlobalPose(p.toEigen());
    }

    void SimulatedObjectAsObject::updatePoseToSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        PosePtr pose(new Pose(localCopy_->getGlobalPose()));
        simulator->setObjectPose(getInstanceName(), pose);
    }

    bool SimulatedObjectAsObject::checkCollision(const VirtualRobot::CollisionCheckerPtr& col,
                                                 const VirtualRobot::SceneObjectSetPtr& objectSet)
    {
        return col->checkCollision(localCopy_->getCollisionModel(), objectSet);
    }

    std::unique_ptr<Pose> SimulatedObjectAsObject::getLocalPose()
    {
        return std::make_unique<Pose>(localCopy_->getGlobalPose());
    }

    void SimulatedObjectAsObject::setLocalPose(const armarx::PosePtr& pose)
    {
        localCopy_->setGlobalPose(pose->toEigen());
    }
}  // namespace armarx::simulation::scene_generation
