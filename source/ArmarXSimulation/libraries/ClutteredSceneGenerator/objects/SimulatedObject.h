/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::simulation::scene_generation
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/PackagePath.h>

#include <VirtualRobot/CollisionDetection/CollisionChecker.h>

namespace armarx::simulation::scene_generation
{
    enum SimulatedObjectType
    {
        AsObject,
        AsRobot,
        Automatic
    };

    struct ObjectSource
    {
        armarx::PackagePath path;
        SimulatedObjectType type;

        std::optional<armarx::ObjectID> objectID;
    };

    struct ObjectSet
    {
        std::vector<ObjectSource> objects;
    };

    class SimulatedObject
    {
        public:
            SimulatedObject() = delete;
            SimulatedObject(const std::string& instanceName, const ObjectSource& objectSource) :
                instanceName_(instanceName),
                objectSource_(objectSource)
            {}

            virtual void addToSimulator(armarx::SimulatorInterfacePrx& simulator) = 0;
            virtual void updatePoseFromSimulator(armarx::SimulatorInterfacePrx& simulator) = 0;
            virtual void updatePoseToSimulator(armarx::SimulatorInterfacePrx& simulator) = 0;

            virtual bool checkCollision(const VirtualRobot::CollisionCheckerPtr& col,
                                        const VirtualRobot::SceneObjectSetPtr& objectSet) = 0;
            
            virtual std::unique_ptr<Pose> getLocalPose() = 0;
            virtual void setLocalPose(const armarx::PosePtr& pose) = 0;

            virtual ~SimulatedObject() = default;

            const std::string& getInstanceName()
            {
                return instanceName_;
            }

            void setInstanceName(const std::string& newName)
            {
                instanceName_ = newName;
            }

            const ObjectSource& getObjectSource()
            {
                return objectSource_;
            }

        private:
            std::string instanceName_;
            const ObjectSource& objectSource_;
    };
}  // namespace armarx::simulation::scene_generation
