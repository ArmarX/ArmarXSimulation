/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::simulation::scene_generation
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimulatedObjectAsRobot.h"

#include <ArmarXSimulation/libraries/ClutteredSceneGenerator/objects/SimulatedObject.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <VirtualRobot/XML/RobotIO.h>

#include <Eigen/src/Core/Matrix.h>

#include <memory>

namespace armarx::simulation::scene_generation
{
    SimulatedObjectAsRobot::SimulatedObjectAsRobot(const std::string& instanceName, const ObjectSource& objectSource) :
        SimulatedObject(instanceName, objectSource),
        localCopy_(VirtualRobot::RobotIO::loadRobot(objectSource.path.toSystemPath()))
    {
        localCopy_->setName(instanceName);
    }

    void SimulatedObjectAsRobot::addToSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        const std::string& newName = simulator->addRobot(getObjectSource().path.toSystemPath());
        setInstanceName(newName);
    }

    void SimulatedObjectAsRobot::updatePoseFromSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        const PoseBasePtr& poseBase = simulator->getRobotPose(getInstanceName());
        Pose p(poseBase->position, poseBase->orientation);
        localCopy_->setGlobalPose(p.toEigen());
    }

    void SimulatedObjectAsRobot::updatePoseToSimulator(armarx::SimulatorInterfacePrx& simulator)
    {
        PosePtr pose(new Pose(localCopy_->getGlobalPose()));
        simulator->setRobotPose(getInstanceName(), pose);
    }

    bool SimulatedObjectAsRobot::checkCollision(const VirtualRobot::CollisionCheckerPtr& col,
                                                const VirtualRobot::SceneObjectSetPtr& objectSet)
    {
        bool collision = false;
        for (const auto& colModel : localCopy_->getCollisionModels())
        {
            collision |= col->checkCollision(colModel, objectSet);
        }
        return collision;
    }

    std::unique_ptr<Pose> SimulatedObjectAsRobot::getLocalPose()
    {
        return std::make_unique<Pose>(localCopy_->getGlobalPose());
    }

    void SimulatedObjectAsRobot::setLocalPose(const armarx::PosePtr& pose)
    {
        localCopy_->setGlobalPose(pose->toEigen());
    }
}  // namespace armarx::simulation::scene_generation
