/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// Qt
#include <QMainWindow>
#include <QDir>
#include <QList>
#include <QStringList>
#include <QPluginLoader>
#include <QDialog>
#include <QComboBox>
#include <QGridLayout>
#include <QStackedWidget>
#include <QSplitter>
#include <QPlainTextEdit>
#include <QMdiArea>
#include <QMutex>
#include <QSignalMapper>
#include <QActionGroup>
#include <QModelIndex>
#include <QListWidget>
#include <QDockWidget>
#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QProxyStyle>
#include <QPainter>
#include <QVector3D>

#ifdef foreach
#undef foreach
#endif

// Coin3D & SoQt
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

// ArmarX
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/CoinViewer.h>

#include <SimDynamics/DynamicsEngine/DynamicsEngine.h>
#include <SimDynamics/DynamicsWorld.h>
#include <SimDynamics/DynamicsEngine/BulletEngine/BulletEngine.h>
#include <SimDynamics/DynamicsEngine/DynamicsRobot.h>
#include <SimDynamics/DynamicsEngine/DynamicsObject.h>

#include <ArmarXSimulation/components/ArmarXPhysicsWorldVisualization/ArmarXPhysicsWorldVisualization.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <ArmarXSimulation/applications/SimulatorViewer/ui_ArmarXSimulatorWindow.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>


// project
namespace armarx
{

    class CoinViewer;


    /*!
     * \class ArmarXSimulatorWindowPropertyDefinitions
     * \brief
     */
    class ArmarXSimulatorWindowPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ArmarXSimulatorWindowPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("ShowBaseCoordSystem", false, "Enable/disbale a coordinate visualization at the global root.");
            defineOptionalProperty<int>("AntiAliasingRenderPasses", 4, "If >0 anti aliasing is enabled (may slow down the visualization). Defines the number of render passes.");

            defineOptionalProperty<float>("Camera.x", 0.f, "x component of initial camera position (mm)");
            defineOptionalProperty<float>("Camera.y", 0.f, "y component of initial camera position (mm)");
            defineOptionalProperty<float>("Camera.z", 0.f, "z component of initial camera position (mm)");
            defineOptionalProperty<float>("Camera.roll", 0.f, "Initial camera pose: roll component of RPY angles (radian)");
            defineOptionalProperty<float>("Camera.pitch", 0.f, "Initial camera pose: pitch component of RPY angles (radian)");
            defineOptionalProperty<float>("Camera.yaw", 0.f, "Initial camera pose: yaw component of RPY angles (radian)");
            defineOptionalProperty<std::string>("TempDir", "/tmp", "Local temp directory. Used for storing video files.");
            defineOptionalProperty<bool>("SaveVideo", false, "Enable/disbale video recording (video is stored to tmp directory, currently only BMP files are supported.");
            defineOptionalProperty<float>("Background.r", 1.f, "r component of initial background color [0, 1]");
            defineOptionalProperty<float>("Background.g", 1.f, "g component of initial background color [0, 1]");
            defineOptionalProperty<float>("Background.b", 1.f, "b component of initial background color [0, 1]");
        }
    };



    class ArmarXSimulatorWindow :
        public QMainWindow,
        virtual public SimulatorViewerControlInterface,
        virtual public Component
    {
        Q_OBJECT

    public:
        ArmarXSimulatorWindow();
        ~ArmarXSimulatorWindow() override;

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "ArmarXSimulatorWindow";
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new ArmarXSimulatorWindowPropertyDefinitions(
                                              getConfigIdentifier()));
        }


        void initialize(ArmarXPhysicsWorldVisualizationPtr physicsVisu, DebugDrawerComponentPtr debugDrawer);

        /**
        * emits the closeRequest signal
        */
        void closeEvent(QCloseEvent* event) override;

        // called by world (when its safe to access scene graph)
        //void updateContacts(std::vector<SimDynamics::DynamicsEngine::DynamicsContactInfo> &c);

        Eigen::Matrix4f getCamPose();

        CoinViewer* getViewer();

        SoNode* getScene();

        /*!
         * \brief Protect access with this lock
         * \return The lock that is automatically destructed when leaving the current scope.
         */
        CoinViewer::RecursiveMutexLockPtr getScopedLock();

        void setMutex(CoinViewer::RecursiveMutexPtr const& m);




        /* Inherited from SimulatorViewerControlInterface. */
        void enableSimulatorWindow(bool show, const Ice::Current& c = Ice::emptyCurrent) override;
        void showDebugDrawLayer(bool show, const Ice::Current& c = Ice::emptyCurrent) override;
        void clearDebugDrawLayer(const Ice::Current& c = Ice::emptyCurrent) override;

        void clearLayer(const std::string& layerName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void removeLayer(const std::string& layerName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void enableLayerVisu(const std::string& layerName, bool visible, const ::Ice::Current& = Ice::emptyCurrent) override;
        ::armarx::LayerInformationSequence layerInformation(const ::Ice::Current& = Ice::emptyCurrent) override;

        void setTempPath(const std::string& p, const ::Ice::Current& = Ice::emptyCurrent) override;
        void saveScreenshots(bool enable, const ::Ice::Current& = Ice::emptyCurrent) override;


        //! Switch between full/collision model
        void selectVisuType(bool fullModel, const Ice::Current& c = Ice::emptyCurrent) override;
        void showBaseCoordSystem(bool show, float scale = 1.0, const Ice::Current& c = Ice::emptyCurrent) override;
        //void showContacts(bool show, const Ice::Current& c = Ice::emptyCurrent);
        SimulatorViewerInformation getSimulatorInformation(const Ice::Current& c = Ice::emptyCurrent) override;

        void setAntiAliasing(int steps, const Ice::Current& c = Ice::emptyCurrent) override;

        PoseBasePtr getCameraPose(const Ice::Current& c = Ice::emptyCurrent) override;

        void setDrawTimeMeasured(float ms);

        void setCamPose(float x, float y, float z, float roll, float pitch, float yaw);
        void setCamPoseFromConfig();

    public slots:
        void slotEnableDebugDrawLayer(bool show);
        void slotClearDebugDrawLayer();
        void slotClearLayer(const QString layerName);
        void slotRemoveLayer(const QString layerName);
        void slotEnableLayerVisu(const QString layerName, bool visible);
        void slotSelectVisuType(bool visible);

        void viewAll();

        // setup anti aliasing passes (0 == disable)
        void setupAntiAliasing(int numPasses);

        void slotShowBaseCoord(bool show, float scale);

    signals:
        /**
        * emitted, when the main window should be closed
        */
        void closeRequest();

    protected:
        Ui::MainWindowArmarXSimulator ui;

        CoinViewer* viewer;

        ArmarXPhysicsWorldVisualizationPtr physicsVisu;
        DebugDrawerComponentPtr debugDrawer;

        void setupUI();


        static void timerCB(void* data, SoSensor* sensor);
        void saveScreenshot();

        SoSeparator* sceneSep;

        CoinViewer::RecursiveMutexPtr mutex;

        armarx::SimulatorViewerInformation info;

        float drawTimeMS;

        bool saveVideo = false;
        std::string tmpDir;

    private:
        QString guiWindowBaseName;
    };


    using ArmarXSimulatorWindowPtr = IceInternal::Handle<ArmarXSimulatorWindow>;
}
