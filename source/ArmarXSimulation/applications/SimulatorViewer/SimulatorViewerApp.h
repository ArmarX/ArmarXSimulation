/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::application::SimulatorViewer
 * @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include <ArmarXCore/core/application/Application.h>

#include <QApplication>

#include "ArmarXSimulatorWindow.h"
#include <ArmarXSimulation/components/ArmarXPhysicsWorldVisualization/ArmarXPhysicsWorldVisualization.h>
#include <MemoryX/components/EntityDrawer/EntityDrawerComponent.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/CoinViewer.h>


namespace armarx
{
    class ARMARXCORE_IMPORT_EXPORT SimulatorViewerAppPropertyDefinitions:
        public ApplicationPropertyDefinitions
    {
    public:
        /**
         * @see armarx::PropertyDefinitionContainer::PropertyDefinitionContainer()
         */
        SimulatorViewerAppPropertyDefinitions(std::string prefix) : ApplicationPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("UpdateRate", 30.0f, "Rate at which the data is polled from the simulator");
            defineOptionalProperty<bool>("UseDebugDrawer", false,
                    "Whether to create the debug drawer component for the viewer.");
        }
    };


    /**
     * @class SimulatorViewerApp
     * @brief A viewer that visualizes the content of the ArmarX Simulator.
     *
     * The application creates an armarx::ArmarXPhysicsWorldVisualization component which connects to the SimulatorVisuUpdate topic
     * in order to retrieve the visualization stream of the current simulated environment. The simulator data stream is continously processed and
     * the visualization is updated accordingly. The viewer is capable of generating a complete visualization at any time.
     * Hence, it can be started from any PC in the network at any time.
     *
     *
     *
     */
    class SimulatorViewerApp :
        public QObject,
        virtual public armarx::Application
    {
        Q_OBJECT
    public:
        /**
        * Constructor
        */
        SimulatorViewerApp();

        ~SimulatorViewerApp() override;

        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties) override;

        /**
        * Runs the Qt Event Loop
        */
        int exec(const ArmarXManagerPtr& armarXManager) override;

        /*!
         * \brief interruptCallback Recieve interrupt callbacks to gracefully shut down simulator
         * \param signal
         */
        void interruptCallback(int signal) override;

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new SimulatorViewerAppPropertyDefinitions(getDomainName()));
        }
    private slots:
        void closeRequest_sent();

    protected:
        QCoreApplication* qApplication;
        ArmarXSimulatorWindowPtr mainWindow;
        memoryx::EntityDrawerComponentPtr debugDrawer;

        ArmarXPhysicsWorldVisualizationPtr simVisu;

        // the one and only mutex
        CoinViewer::RecursiveMutexPtr mutex;

        bool exitApplication;
    };
}
