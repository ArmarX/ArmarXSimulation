armarx_component_set_name("FakeWorkingMemoryObjectLocalizerApp")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore FakeWorkingMemoryObjectLocalizer)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
