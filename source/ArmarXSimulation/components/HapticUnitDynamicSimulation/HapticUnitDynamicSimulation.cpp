/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "HapticUnitDynamicSimulation.h"

#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;


void armarx::HapticUnitDynamicSimulation::onInitHapticUnit()
{
    ARMARX_IMPORTANT << "onInitHapticUnit()";
    sensorTask = new RunningTask<HapticUnitDynamicSimulation>(this, &HapticUnitDynamicSimulation::frameAcquisitionTaskLoop);

    std::string simPrxName = getProperty<std::string>("SimulatorName").getValue();
    usingProxy(simPrxName);
    robotNodeNames = getCommaSeparatedProperty("RobotNodes");
    environmentObjectNames = getCommaSeparatedProperty("EnvironmentObjects");
    robotName = getProperty<std::string>("RobotName").getValue();
    offeringTopic("DebugDrawerUpdates");
    ARMARX_IMPORTANT << "robotNodeNames: " << robotNodeNames;
    ARMARX_IMPORTANT << "environmentObjectNames" << environmentObjectNames;

}

void armarx::HapticUnitDynamicSimulation::onStartHapticUnit()
{
    std::string simPrxName = getProperty<std::string>("SimulatorName").getValue();
    simulatorProxy = getProxy<SimulatorInterfacePrx>(simPrxName);
    ARMARX_CHECK_EXPRESSION(simulatorProxy);

    debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>("DebugDrawerUpdates");

    sensorTask->start();
}

void armarx::HapticUnitDynamicSimulation::onExitHapticUnit()
{
    sensorTask->stop();
}

PropertyDefinitionsPtr HapticUnitDynamicSimulation::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new HapticUnitDynamicSimulationPropertyDefinitions(getConfigIdentifier()));
}

void HapticUnitDynamicSimulation::frameAcquisitionTaskLoop()
{
    while (!sensorTask->isStopped())
    {
        for (size_t i = 0; i < robotNodeNames.size(); i++)
        {
            //float minValue = 1e10;
            std::string robotNodeName = robotNodeNames.at(i);

            for (std::string objectName : environmentObjectNames)
            {
                DistanceInfo di = simulatorProxy->getDistance(robotName, robotNodeName, objectName);
                ARMARX_IMPORTANT << "Distance between " << robotNodeName << " and " << objectName << " = " << di.distance;
                ARMARX_IMPORTANT << "p1:" << di.p1 << ", p2:" << di.p2;
                debugDrawerPrx->setLineDebugLayerVisu(robotNodeName + objectName, di.p1, di.p2, 5.0f, DrawColor {1, 0, 0, 1});
            }
        }

        MatrixFloatPtr matrix = new MatrixFloat(7, 5); // y,x
        TimestampVariantPtr now = TimestampVariant::nowPtr();
        (*matrix)(0, 0) = (now->getTimestamp() / 500) % 4096; // generate some sample data
        (*matrix)(0, 1) = (now->getTimestamp() / 1000) % 4096; // generate some sample data
        (*matrix)(0, 2) = (now->getTimestamp() / 2000) % 4096; // generate some sample data
        hapticTopicPrx->reportSensorValues("/virtualdev/sensorA", "Left Index", matrix, now);
        TimeUtil::MSSleep(1);
    }
}

std::vector<std::string> HapticUnitDynamicSimulation::getCommaSeparatedProperty(std::string propertyName)
{
    std::string strValue = getProperty<std::string>(propertyName).getValue();
    std::vector<std::string> values = simox::alg::split(strValue, ",");

    for (size_t i = 0; i < values.size(); i++)
    {
        simox::alg::trim(values.at(i));
    }

    return values;
}
