/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/components/units/HapticUnit.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

namespace armarx
{
    class HapticUnitDynamicSimulationPropertyDefinitions : public HapticUnitPropertyDefinitions
    {
    public:
        HapticUnitDynamicSimulationPropertyDefinitions(std::string prefix):
            HapticUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("SimulatorName", "Simulator", "Name of the simulator component that should be used");
            defineOptionalProperty<std::string>("RobotNodes", "Index R J1, Middle R J1", "Comma separated list of robot nodes that are used to determine contact information (RobotNodes must own a collision model)");
            defineOptionalProperty<std::string>("EnvironmentObjects", "Vitalis", "Comma separated list of world objects that are used to determine contact information");
            defineOptionalProperty<std::string>("RobotName", "Armar3", "Name of the robot to use.");
        }
    };

    class HapticUnitDynamicSimulation : virtual public HapticUnit
    {
        // HapticUnit interface
    public:
        virtual std::string getDefaultName()
        {
            return "HapticUnitDynamicSimulation";
        }

        void onInitHapticUnit() override;
        void onStartHapticUnit() override;
        void onExitHapticUnit() override;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        RunningTask<HapticUnitDynamicSimulation>::pointer_type sensorTask;
        void frameAcquisitionTaskLoop();
        std::vector<std::string> getCommaSeparatedProperty(std::string propertyName);

        SimulatorInterfacePrx simulatorProxy;
        std::vector<std::string> robotNodeNames;
        std::vector<std::string> environmentObjectNames;
        std::string robotName;

        DebugDrawerInterfacePrx debugDrawerPrx;
    };
}

