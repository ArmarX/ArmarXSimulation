/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::SimulatorToArviz
 * @author     Peter Reiner ( ufekv at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectClassSegmentWrapper.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>


namespace armarx
{

    /**
     * @defgroup Component-SimulatorToArviz SimulatorToArviz
     * @ingroup ArmarXSimulation-Components
     * A description of the component SimulatorToArviz.
     *
     * @class SimulatorToArviz
     * @ingroup Component-SimulatorToArviz
     * @brief Brief description of class SimulatorToArviz.
     *
     * Detailed description of class SimulatorToArviz.
     */
    class SimulatorToArviz :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::SimulatorListenerInterface
    {
    public:

        SimulatorToArviz();
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        void reportSceneUpdated(const SceneVisuData& scene, const ::Ice::Current& = ::Ice::emptyCurrent) override;

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

        void handleObjectPrimitiveData(const ObjectVisuData& _object, armarx::viz::Layer& objectLayer);
        void handleObjectFromObjectClass(const ObjectVisuData& _object, armarx::viz::Layer& objectLayer);
        std::string getClassFilename(const std::string& className);
        void handleObjectFromFile(const ObjectVisuData& _object, armarx::viz::Layer& objectLayer);

        void handleRobotLayer(const RobotVisuList& robotList, armarx::viz::Layer& robotLayer);
        void handleObjectLayer(const ObjectVisuList& objectList, armarx::viz::Layer& objectLayer);

        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;


        struct Properties
        {
            struct OldPriorKnowledge
            {
                bool use = false;
                std::string name = "PriorKnowledge";
            };
            OldPriorKnowledge oldPriorKnowledge;
        };
        Properties properties;

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::ObjectClassSegmentWrapper objectClassSegment;

        std::map<std::string, std::string> classFilenameCache;

        //SimulatorListenerInterfacePrx _simulatorListenerInterfacePrx;
    };
}
