/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::units
 * @author    Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
 * @date      2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/HandUnit.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <IceUtil/Time.h>

#include <string>
#include <cmath>

namespace armarx
{
    /**
     * @class HandUnitDynamicSimulationPropertyDefinitions
     * @brief
     * @ingroup SensorActorUnits
     */
    class HandUnitDynamicSimulationPropertyDefinitions :
        public HandUnitPropertyDefinitions
    {
    public:
        HandUnitDynamicSimulationPropertyDefinitions(std::string prefix) :
            HandUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("SimulatorProxyName", "Simulator", "Name of the simulator proxy to use.");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");
            defineOptionalProperty<bool>("UseLegacyWorkingMemory", false,
                    "Require the legacy MemoryX working memory to be available before starting.");
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
        }
    };

    /**
    * @class HandUnitDynamicSimulation
    * @brief This unit connects to the physics simulator topic (default: "Simulator") and implements a HandUnit.
    *
    * @ingroup SensorActorUnits
    * @ingroup ArmarXSimulatorComponents
    */
    class HandUnitDynamicSimulation :
        virtual public HandUnit
    //, virtual public HandUnitDynamicSimulationInterface
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "HandUnitDynamicSimulation";
        }

        void onInitHandUnit() override;
        void onStartHandUnit() override;
        void onExitHandUnit() override;

        /**
         * Send command to the hand to open all fingers.
         */
        //virtual void open(const Ice::Current&);


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void setObjectGrasped(const std::string& objectName, const Ice::Current&) override;
        void setObjectReleased(const std::string& objectName, const Ice::Current&) override;

        NameValueMap getCurrentJointValues(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:

        std::string simulatorPrxName;

        SimulatorInterfacePrx simulatorPrx;

        KinematicUnitInterfacePrx kinematicUnitPrx;

        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;

        std::string robotStateComponentName;
        RobotStateComponentInterfacePrx robotStateComponentPrx;

        std::string objectPoseStorageName = "ObjectMemory";

        armarx::NameValueMap handUnitJointsToRobotJoints(const armarx::NameValueMap& joints);
        std::string getRobotNameFromHandUnitName();
        std::string getHandSideFromHandUnitName();

        // HandUnitInterface interface
    public:
        void setShape(const std::string& shapeName, const Ice::Current&) override;
        void setShapeWithObjectInstance(const std::string& shapeName, const std::string& objectInstanceName, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointAngles(const NameValueMap& jointAngles, const Ice::Current&) override;
    };
}

