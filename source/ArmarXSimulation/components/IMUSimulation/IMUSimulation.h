/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::IMUSimulation
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>



#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/components/units/InertialMeasurementUnit.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace armarx
{
    /**
     * @class IMUSimulationPropertyDefinitions
     * @brief
     */
    class IMUSimulationPropertyDefinitions:
        public InertialMeasurementUnitPropertyDefinitions
    {
    public:
        IMUSimulationPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-IMUSimulation IMUSimulation
     * @ingroup ArmarXSimulation-Components
     * A description of the component IMUSimulation.
     *
     * @class IMUSimulation
     * @ingroup Component-IMUSimulation
     * @brief Brief description of class IMUSimulation.
     *
     * Detailed description of class IMUSimulation.
     */
    class IMUSimulation :
        virtual public InertialMeasurementUnit
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitIMU() override;

        void onStartIMU() override;

        void onExitIMU() override;


    private:


        void frameAcquisitionTaskLoop();


    private:

        PeriodicTask<IMUSimulation>::pointer_type sensorTask;

        Eigen::Vector3f position;
        Eigen::Quaternionf orientation;


        std::string nodeName;
        double timestamp;
        Eigen::Vector3f linearVelocity;


        SimulatorInterfacePrx simulator;
        RobotStateComponentInterfacePrx robotStateComponent;
    };
}

