/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::units
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/PlatformUnit.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <IceUtil/Time.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <string>
#include <cmath>

namespace armarx
{

    /**
     * @class PlatformUnitDynamicSimulation
     * @brief This unit connects to the physics simulator topic (default: "Simulator") and handles platform movements.
     *
     * @ingroup SensorActorUnits
     * @ingroup ArmarXSimulatorComponents
     */
    class PlatformUnitDynamicSimulation :
        virtual public PlatformUnit,
        virtual public PlatformUnitDynamicSimulationInterface
    {
    public:

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "PlatformUnit";
        }

        void onInitPlatformUnit() override;
        void onStartPlatformUnit() override;
        void onStopPlatformUnit() override;
        void onExitPlatformUnit() override;

        void simulationFunction();

        // proxy implementation
        void moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation,  Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;
        void move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation, const Ice::Current& c = Ice::emptyCurrent) override;
        void moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;
        void setMaxVelocities(float linearVelocity, float angularVelocity, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        void updateCurrentPose(const PoseBasePtr& newPose);
        void updateCurrentVelocity(const Vector3BasePtr& translationVel, const Vector3BasePtr& rotationVel);

        // implement SimulatorRobotListener to receive robot updates from simulator
        void reportState(SimulatedRobotState const& state, const Ice::Current&) override;

        //calc acceleration based on distance and current speed
        Eigen::Vector3f calculateLinearVelocity(Eigen::Vector3f distance, Eigen::Vector3f curV);
        float calculateAngularVelocity(float source, float target, float curV);

        float clampf(float x, float min, float max)
        {
            return std::max<float>(std::min<float>(x, max), min);
        }

        std::mutex currentPoseMutex;
        IceUtil::Time lastExecutionTime;
        int intervalMs;

        Vector3 currentPosition;
        Vector3 targetPosition;
        ::Ice::Float currentRotation;

        ::Ice::Float targetRotation;

        Vector3 currentVelocity;
        ::Ice::Float currentRotationVelocity;

        ::Ice::Float linearVelocityMin;
        ::Ice::Float linearVelocityMax;
        ::Ice::Float linearAccelerationMax;
        ::Ice::Float platformOrientationOffset;

        ::Ice::Float angularVelocityMax;
        ::Ice::Float angularAccelerationMax;

        ::Ice::Float positionalAccuracy;
        ::Ice::Float orientationalAccuracy;

        std::string referenceFrame;
        std::string simulatorPrxName;
        std::string robotName;
        std::string platformName;

        ControlMode platformControlMode;


        SimulatorInterfacePrx simulatorPrx;

        PeriodicTask<PlatformUnitDynamicSimulation>::pointer_type simulationTask;

        // PlatformUnitInterface interface
    public:
        void stopPlatform(const Ice::Current& current) override;
    };
}

