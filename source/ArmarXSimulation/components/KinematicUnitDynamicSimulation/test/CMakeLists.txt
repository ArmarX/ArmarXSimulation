
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore KinematicUnitDynamicSimulation)
 
armarx_add_test(KinematicUnitDynamicSimulationTest KinematicUnitDynamicSimulationTest.cpp "${LIBS}")