/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::KinematicUnitDynamicSimulation
 * @author     Nikolaus ( vahrenkamp at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/components/units/KinematicUnitSimulation.h>

#include <SimDynamics/DynamicsEngine/DynamicsRobot.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <set>

#include <mutex>

namespace armarx
{

    class KinematicUnitDynamicSimulationPropertyDefinitions:
        public armarx::KinematicUnitPropertyDefinitions
    {
    public:
        KinematicUnitDynamicSimulationPropertyDefinitions(std::string prefix):
            KinematicUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("MapVelocityToPositionVelocity", false, "If set, velocity control mode is internally handled as position velocity mode.");
            defineOptionalProperty<std::string>("RobotName", "", "Name of the robot");
            defineOptionalProperty<std::string>("SimulatorName", "Simulator", "Name of the simulator component that should be used");
            defineOptionalProperty<float>("ControlFrequency", 100, "Frequency of the control loop");

        }
    };

    /**
     * @class KinematicUnitDynamicSimulation
     * @brief This component implements the KinemticUnit with access to a physics simulator. The Simulator is quried through the simulator interface and connects to the simulator topic with name "ArmarXPhysicsWorld".
     *
     * @ingroup ArmarXSimulatorComponents
     * @ingroup SensorActorUnits
     *
     */
    class KinematicUnitDynamicSimulation :
        virtual public KinematicUnit,
        virtual public KinematicUnitDynamicSimulationInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "KinematicUnitDynamicSimulation";
        }

        void onInitKinematicUnit() override;
        void onStartKinematicUnit() override;
        void onExitKinematicUnit() override;

        // proxy implementation (KinematicUnit)
        void requestJoints(const Ice::StringSeq& joints, const Ice::Current& c = Ice::emptyCurrent) override;
        void releaseJoints(const Ice::StringSeq& joints, const Ice::Current& c = Ice::emptyCurrent) override;
        void switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointTorques(const NameValueMap& targetJointTorques, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointAccelerations(const NameValueMap& targetJointAccelerations, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointDecelerations(const NameValueMap& targetJointDecelerations, const Ice::Current& c = Ice::emptyCurrent) override;

        NameValueMap getJointAngles(const Ice::Current&) const override
        {
            ARMARX_WARNING << "NYI";
            return {};
        }
        NameValueMap getJointVelocities(const Ice::Current&) const override
        {
            ARMARX_WARNING << "NYI";
            return {};
        }
        Ice::StringSeq getJoints(const Ice::Current&) const override
        {
            ARMARX_WARNING << "NYI";
            return {};
        }

        DebugInfo getDebugInfo(const Ice::Current& c = Ice::emptyCurrent) const override;

    protected:

        // implement SimulatorRobotListener to receive robot updates from simulator
        void reportState(SimulatedRobotState const& state, const Ice::Current&) override;

        NameControlModeMap getControlModes(const Ice::Current& c = Ice::emptyCurrent) override;

        PeriodicTask<KinematicUnitDynamicSimulation>::pointer_type execTask;

        void periodicExec();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new KinematicUnitDynamicSimulationPropertyDefinitions(
                           getConfigIdentifier()));
        }

        std::set<std::string> nodeNames;
        std::string robotName;
        std::string robotTopicName;
        std::string simulatorPrxName;

        NameValueMap currentJointTargets;
        NameValueMap currentJointVelTargets;
        NameValueMap currentJointTorqueTargets;
        NameControlModeMap currentControlModes;

        int cycleTime;

        NameValueMap currentJointValues;

        bool mapVelToPosVel;

        std::recursive_mutex accessMutex;

        SimulatorInterfacePrx simulatorPrx;
    };
}
