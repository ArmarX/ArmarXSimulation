/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author     Raphael Grimm
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <chrono>
#include <random>
#include <mutex>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

#include <ArmarXCore/core/Component.h>

#include <VisionX/interface/components/RGBDImageProvider.h>
#include <VisionX/core/ImageProvider.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>

#include <ArmarXSimulation/components/ArmarXPhysicsWorldVisualization/ArmarXPhysicsWorldVisualization.h>



#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

namespace armarx
{
    class DepthImageProviderDynamicSimulationPropertyDefinitions:
        public visionx::CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        DepthImageProviderDynamicSimulationPropertyDefinitions(std::string prefix):
            visionx::CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("FloatImageMode", false, "Whether to provide a CFloatImage or the standard CByteImage");
            defineOptionalProperty<float>("Noise", 0.0f, "Noise of the point cloud position results as standard deviation of the normal distribution (in mm)")
            .setMin(0);
            defineOptionalProperty<float>("DistanceZNear", 20.0f, "Distance of the near clipping plain. (If set too small the agent's model's inside may be visible")
            .setMin(1e-8);
            defineOptionalProperty<float>("DistanceZFar", 5000.0f, "Distance of the far clipping plain. (DistanceZFar-DistanceZNear should be minimal, DistanceZFar > DistanceZNear)")
            .setMin(1e-8);
            defineOptionalProperty<float>("FOV", M_PI / 4, "Vertical FOV in rad.");
            defineOptionalProperty<float>("BaseLine", 0.075, "The value returned from getBaseline(). It has no other effect.");
            defineOptionalProperty<float>("NanValue", std::nan(""), "Value of points that are farther away then DistanceZFar. Most cameras return here NaN.");

            defineOptionalProperty<visionx::ImageDimension>("ImageSize", visionx::ImageDimension(640,  480), "Target resolution of the images. Captured images will be converted to this size.")
            .setCaseInsensitive(true)
            .map("200x200",    visionx::ImageDimension(200, 200))
            .map("320x240",     visionx::ImageDimension(320,  240))
            .map("640x480",     visionx::ImageDimension(640,  480))
            .map("800x600",     visionx::ImageDimension(800,  600))
            .map("768x576",     visionx::ImageDimension(768,  576))
            .map("1024x768",    visionx::ImageDimension(1024, 768))
            .map("1280x960",    visionx::ImageDimension(1280, 960))
            .map("1600x1200",   visionx::ImageDimension(1600, 1200))
            .map("none",        visionx::ImageDimension(0, 0));
            defineOptionalProperty<std::string>("RobotName", "Armar3", "The robot");
            defineOptionalProperty<std::string>("RobotNodeCamera", "DepthCameraSim", "The coordinate system of the used camera");

            //used to draw the cloud to the simulator
            defineOptionalProperty<bool>("DrawPointCloud", false, "Whether the point cloud is drawn to the given DebugDrawerTopic");
            defineOptionalProperty<std::string>("DrawPointCloud_DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<unsigned int>("DrawPointCloud_DrawDelay", 1000, "The time between updates of the drawn point cloud (in ms)");
            defineOptionalProperty<float>("DrawPointCloud_PointSize", 4, "The size of a point.");
            defineOptionalProperty<std::size_t>("DrawPointCloud_PointSkip", 3, "Only draw every n'th point in x and y direction (n=DrawPointCloud_PointSkip). Increase this whenever the ice buffer size is to small to transmitt the cloud size. (>0)");

            defineOptionalProperty<bool>("DrawPointCloud_ClipPoints", true, "Whether to clip the point cloud drawn to the given DebugDrawerTopic");

            defineOptionalProperty<float>("DrawPointCloud_ClipXHi", 25000, "Skip points with x higher than this limit.");
            defineOptionalProperty<float>("DrawPointCloud_ClipYHi", 25000, "Skip points with y higher than this limit.");
            defineOptionalProperty<float>("DrawPointCloud_ClipZHi", 25000, "Skip points with z higher than this limit.");

            defineOptionalProperty<float>("DrawPointCloud_ClipXLo", -25000, "Skip points with x lower than this limit.");
            defineOptionalProperty<float>("DrawPointCloud_ClipYLo", -25000, "Skip points with y lower than this limit.");
            defineOptionalProperty<float>("DrawPointCloud_ClipZLo", -25000, "Skip points with z lower than this limit.");
        }
    };

    class DepthImageProviderDynamicSimulation :
        virtual public visionx::CapturingPointCloudProvider,
        virtual public visionx::ImageProvider,
        virtual public visionx::RGBDPointCloudProviderInterface
    {
    public:

        DepthImageProviderDynamicSimulation() = default;
        ~DepthImageProviderDynamicSimulation() override {}

        std::string getDefaultName() const override
        {
            return "DynamicSimulationDepthImageProvider";
        }

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override
        {
            visionx::StereoCalibration stereoCalibration;


            float r = static_cast<float>(width) / static_cast<float>(height);

            visionx::CameraParameters RGBCameraIntrinsics;
            RGBCameraIntrinsics.distortion = {0, 0, 0, 0};
            RGBCameraIntrinsics.focalLength = {static_cast<float>(width) / (2 * std::tan((vertFov * r) / 2)),
                                               static_cast<float>(height) / (2 * std::tan(vertFov / 2))
                                              };
            RGBCameraIntrinsics.height = height;
            RGBCameraIntrinsics.principalPoint = {width / 2.0f, height / 2.0f};
            RGBCameraIntrinsics.rotation = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Identity());
            RGBCameraIntrinsics.translation = visionx::tools::convertEigenVecToVisionX(Eigen::Vector3f::Zero());
            RGBCameraIntrinsics.width = width;

            visionx::CameraParameters DepthCameraIntrinsics;
            DepthCameraIntrinsics.distortion = {0, 0, 0, 0};
            DepthCameraIntrinsics.focalLength = {static_cast<float>(width) / (2 * std::tan((vertFov * r) / 2)),
                                                 static_cast<float>(height) / (2 * std::tan(vertFov / 2))
                                                };
            DepthCameraIntrinsics.height = height;
            DepthCameraIntrinsics.principalPoint = {width / 2.0f, height / 2.0f};
            DepthCameraIntrinsics.rotation =  visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Identity());
            DepthCameraIntrinsics.translation = {0.075, 0, 0};
            DepthCameraIntrinsics.width = width;


            stereoCalibration.calibrationLeft = visionx::tools::createDefaultMonocularCalibration();
            stereoCalibration.calibrationRight = visionx::tools::createDefaultMonocularCalibration();
            stereoCalibration.calibrationLeft.cameraParam = RGBCameraIntrinsics;
            stereoCalibration.calibrationRight.cameraParam = DepthCameraIntrinsics;
            stereoCalibration.rectificationHomographyLeft = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Zero());
            stereoCalibration.rectificationHomographyRight = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Zero());


            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("RobotNodeCamera");
        }

    protected:
        /**
         * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
         */
        void onInitCapturingPointCloudProvider() override {}

        /**
         * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
         */
        void onExitCapturingPointCloudProvider() override;

        /**
         * @see visionx::PointCloudProviderBase::onStartCapture()
         */
        void onStartCapture(float frameRate) override;

        /**
         * @see visionx::PointCloudProviderBase::onStopCapture()
         */
        void onStopCapture() override {}

        /**
         * @see visionx::PointCloudProviderBase::doCapture()
        */
        bool doCapture() override;

        /**
             * @see visionx::CapturingImageProvider::onInitImageProvider()
             */
        void onInitImageProvider() override;

        void onDisconnectComponent() override;

        /**
             * @see visionx::CapturingImageProvider::onExitImageProvider()
             */
        void onExitImageProvider() override {}


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return new DepthImageProviderDynamicSimulationPropertyDefinitions(getConfigIdentifier());
        }

        // mixed inherited stuff
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        bool render();

        void syncVisu();

        std::mutex captureMutex;

        short width; //coin uses short here
        short height;//coin uses short here

        float baseline;

        ArmarXPhysicsWorldVisualizationPtr simVisu;

        std::string robotName;
        std::string camNodeName;
        VirtualRobot::RobotNodePtr cameraNode;
        SoSeparator* visualization{nullptr};

        bool floatImageMode{false};

        std::unique_ptr<CByteImage> rgbImage;
        std::unique_ptr<CByteImage> dImage;
        std::unique_ptr<CFloatImage> dFloatImage;
        CByteImage* imgPtr[2];
        CFloatImage* imgPtrFlt[1];
        std::vector<unsigned char> rgbBuffer;
        std::vector<float> depthBuffer;
        std::vector<Eigen::Vector3f> pointCloudBuffer;

        float zNear;
        float zFar;
        float vertFov;
        float focalLength;
        float nanValue;

        using PointT = pcl::PointXYZRGBA;
        pcl::PointCloud<PointT>::Ptr pointcloud;

        //draw to debug drawer
        bool drawPointCloud;
        std::string debugDrawerTopicName;
        DebugDrawerInterfacePrx debugDrawer;
        unsigned int pointCloudDrawDelay;
        std::chrono::high_resolution_clock::time_point lastUpdate;
        std::size_t pointCloudPointSize;

        bool clipPointCloud;

        float clipDrawnCloudXLo;
        float clipDrawnCloudYLo;
        float clipDrawnCloudZLo;

        float clipDrawnCloudXHi;
        float clipDrawnCloudYHi;
        float clipDrawnCloudZHi;

        std::size_t pointSkip;

        float noise = 0.0f;
        std::vector<float> randValues;
        std::normal_distribution<double> distribution;
        std::default_random_engine generator;
        std::shared_ptr<SoOffscreenRenderer> offscreenRenderer;

        std::int64_t sumRenderTimes;
        std::int64_t sumRenderTimesSquared;
        std::int32_t renderTimesCount;
    };
}


