/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author     Raphael Grimm
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DepthImageProviderDynamicSimulation.h"

#include <Eigen/Geometry>

#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/SoInteraction.h>

#include <pcl/common/transforms.h>
#include <pcl/compression/organized_pointcloud_conversion.h>

#include <ArmarXCore/util/CPPUtility/trace.h>

namespace armarx
{
    void DepthImageProviderDynamicSimulation::onInitComponent()
    {
        ARMARX_TRACE;
        ARMARX_VERBOSE_S << "DepthImageProviderDynamicSimulation::onInitComponent";
        baseline = getProperty<float>("BaseLine").getValue();
        robotName = getProperty<std::string>("RobotName").getValue();
        camNodeName = getProperty<std::string>("RobotNodeCamera").getValue();

        zNear = getProperty<float>("DistanceZNear").getValue();
        zFar = getProperty<float>("DistanceZFar").getValue();
        vertFov = getProperty<float>("FOV").getValue();
        nanValue = getProperty<float>("NanValue").getValue();

        floatImageMode = getProperty<bool>("FloatImageMode").getValue();

        drawPointCloud = getProperty<bool>("DrawPointCloud").getValue();
        if (drawPointCloud)
        {
            lastUpdate = std::chrono::high_resolution_clock::now() - std::chrono::seconds {pointCloudDrawDelay};
            debugDrawerTopicName = getProperty<std::string>("DrawPointCloud_DebugDrawerTopic").getValue();
            offeringTopic(debugDrawerTopicName);
            pointCloudDrawDelay = getProperty<unsigned int>("DrawPointCloud_DrawDelay").getValue();
            pointCloudPointSize = getProperty<float>("DrawPointCloud_PointSize").getValue();

            pointSkip =  getProperty<std::size_t>("DrawPointCloud_PointSkip").getValue();

            clipDrawnCloudXLo = getProperty<float>("DrawPointCloud_ClipXLo").getValue();
            clipDrawnCloudYLo = getProperty<float>("DrawPointCloud_ClipYLo").getValue();
            clipDrawnCloudZLo = getProperty<float>("DrawPointCloud_ClipZLo").getValue();

            clipDrawnCloudXHi = getProperty<float>("DrawPointCloud_ClipXHi").getValue();
            clipDrawnCloudYHi = getProperty<float>("DrawPointCloud_ClipYHi").getValue();
            clipDrawnCloudZHi = getProperty<float>("DrawPointCloud_ClipZHi").getValue();

            clipPointCloud = getProperty<bool>("DrawPointCloud_ClipPoints").getValue();
        }
        noise = getProperty<float>("Noise").getValue();
        if (noise > 0)
        {
            distribution = std::normal_distribution<double>(0, noise);
            randValues.resize(1000, 0);
            for (float& val : randValues)
            {
                val = distribution(generator);
            }
        }

        ImageProvider::onInitComponent();
        CapturingPointCloudProvider::onInitComponent();
        ARMARX_VERBOSE_S << "done DepthImageProviderDynamicSimulation::onInitComponent";
    }

    void DepthImageProviderDynamicSimulation::onConnectComponent()
    {
        ARMARX_TRACE;
        ARMARX_VERBOSE_S << "DepthImageProviderDynamicSimulation::onConnectComponent";

        if (drawPointCloud)
        {
            debugDrawer = getTopic<DebugDrawerInterfacePrx>(debugDrawerTopicName);
        }

        ImageProvider::onConnectComponent();
        CapturingPointCloudProvider::onConnectComponent();
        ARMARX_VERBOSE_S << "done DepthImageProviderDynamicSimulation::onConnectComponent";
    }

    void DepthImageProviderDynamicSimulation::onExitComponent()
    {
        ARMARX_VERBOSE_S << "DepthImageProviderDynamicSimulation::onExitComponent";
        CapturingPointCloudProvider::onExitComponent();
        ImageProvider::onExitComponent();
        ARMARX_VERBOSE_S << "done DepthImageProviderDynamicSimulation::onExitComponent";
    }

    void DepthImageProviderDynamicSimulation::onStartCapture(float frameRate)
    {
        ARMARX_TRACE;
        ARMARX_INFO << "DepthImageProviderDynamicSimulation::onStartCapture";
        // ensure that all data is loaded
        while (!simVisu || !simVisu->getRobot(robotName))
        {
            if (simVisu)
            {
                ARMARX_TRACE;
                simVisu->synchronizeVisualizationData();
            }
            usleep(100000);
            ARMARX_INFO << deactivateSpam(3) << "Waiting for visu";
        }
        syncVisu();
        ARMARX_INFO << "Got visu";

        ARMARX_TRACE;
        std::unique_lock lock(captureMutex);
        auto l = simVisu->getScopedLock();

        // some checks...
        ARMARX_TRACE;
        offscreenRenderer.reset(VirtualRobot::CoinVisualizationFactory::createOffscreenRenderer(width, height));
        ARMARX_INFO << "done DepthImageProviderDynamicSimulation::onStartCapture";
    }



    void DepthImageProviderDynamicSimulation::onExitCapturingPointCloudProvider()
    {
        ARMARX_TRACE;
        rgbImage.reset();
        dImage.reset();
        dFloatImage.reset();
        imgPtr[0] = nullptr;
        imgPtr[1] = nullptr;
        imgPtrFlt[0] = nullptr;

        if (simVisu)
        {
            simVisu->releaseResources();
            getArmarXManager()->removeObjectBlocking(simVisu);
        }
        simVisu = nullptr;
        cameraNode = nullptr;
        visualization = nullptr;

        offscreenRenderer.reset();

        SoDB::finish();
    }

    void DepthImageProviderDynamicSimulation::onInitImageProvider()
    {
        ARMARX_TRACE;
        ARMARX_VERBOSE_S << "DepthImageProviderDynamicSimulation::onInitImageProvider";

        const auto imgSz = getProperty<visionx::ImageDimension>("ImageSize").getValue();
        height = imgSz.height;
        width = imgSz.width;

        if (!floatImageMode)
        {
            setImageFormat(visionx::ImageDimension(imgSz), visionx::eRgb);
            setNumberImages(2);

            rgbImage.reset(new CByteImage {static_cast<int>(width), static_cast<int>(height), CByteImage::eRGB24});
            dImage.reset(new CByteImage {static_cast<int>(width), static_cast<int>(height), CByteImage::eRGB24});
            imgPtr[0] = rgbImage.get();
            imgPtr[1] = dImage.get();
        }
        else
        {
            setImageFormat(visionx::ImageDimension(imgSz), visionx::eFloat1Channel);
            setNumberImages(1);

            dFloatImage.reset(new CFloatImage {static_cast<int>(width), static_cast<int>(height), 1});
            imgPtrFlt[0] = dFloatImage.get();
        }


        pointcloud.reset(new pcl::PointCloud<PointT> {static_cast<unsigned int>(width), static_cast<unsigned int>(height)});

        rgbBuffer.resize(width * height);
        depthBuffer.resize(width * height);
        pointCloudBuffer.resize(width * height);

        // init SoDB / Coin3D
        SoDB::init();

        VirtualRobot::init(getName());

        // needed for SoSelection
        SoInteraction::init();

        std::stringstream svName;
        svName << getName() << "_PhysicsWorldVisualization";
        simVisu = Component::create<ArmarXPhysicsWorldVisualization>(getIceProperties(), getName() + "_PhysicsWorldVisualization");
        getArmarXManager()->addObject(simVisu);
        ARMARX_VERBOSE_S << "done DepthImageProviderDynamicSimulation::onInitImageProvider";

        renderTimesCount = 0;
        sumRenderTimes = 0;
        sumRenderTimesSquared = 0;
    }

    void DepthImageProviderDynamicSimulation::onDisconnectComponent()
    {
        ARMARX_TRACE;
        ImageProvider::onDisconnectComponent();
        CapturingPointCloudProvider::onDisconnectComponent();
    }

    bool DepthImageProviderDynamicSimulation::doCapture()
    {
        ARMARX_TRACE;
        bool succeeded = true;
        Ice::Long timestamp;
        try
        {
            timestamp = TimeUtil::HasTimeServer() ? simVisu->getSyncTimestamp() : IceUtil::Time::now().toMicroSeconds();
            syncVisu();
            if (!visualization || !visualization)
            {
                return false;
            }
            succeeded = render();
        }
        catch (...)
        {
            handleExceptions();
            succeeded = false;
        }

        if (succeeded)
        {
            Eigen::Matrix4f rotate = Eigen::Matrix4f::Identity();
            // rotate to conform to ArmarX standards
            rotate.block(0, 0, 3, 3) = Eigen::AngleAxis<float>(M_PI_2, Eigen::Vector3f::UnitZ()). toRotationMatrix();
            pcl::PointCloud<PointT>::Ptr cloudTransformedPtr(new pcl::PointCloud<PointT>());
            pcl::transformPointCloud(*pointcloud, *cloudTransformedPtr, rotate);
            cloudTransformedPtr->header.stamp = timestamp;
            providePointCloud(cloudTransformedPtr);

            updateTimestamp(timestamp);
            if (!floatImageMode)
            {
                provideImages(imgPtr);
            }
            else
            {
                provideImages(imgPtrFlt);
            }

            if (drawPointCloud && (std::chrono::high_resolution_clock::now() - lastUpdate > std::chrono::milliseconds {pointCloudDrawDelay}))
            {
                lastUpdate = std::chrono::high_resolution_clock::now();
                DebugDrawer24BitColoredPointCloud dbgPcl;
                dbgPcl.points.reserve(cloudTransformedPtr->size());

                dbgPcl.pointSize = pointCloudPointSize;

                const auto pointCloudBaseNode = simVisu->getRobot(robotName)->getRobotNode(camNodeName);

                if (!pointCloudBaseNode)
                {
                    ARMARX_ERROR_S << deactivateSpam() << robotName << " has no node " << camNodeName
                                   << "\nthe point cloud will be attached to (0,0,0)";
                }

                Eigen::Matrix4f rotate = Eigen::Matrix4f::Identity();
                //                rotate.block(0, 0, 3, 3) = Eigen::AngleAxis<float>(M_PI_2, Eigen::Vector3f::UnitZ()). toRotationMatrix();
                Eigen::Matrix4f transformCamToWorld = (pointCloudBaseNode ? pointCloudBaseNode->getGlobalPose() : Eigen::Matrix4f::Identity()) * rotate;


                assert(static_cast<std::size_t>(width)  == pointcloud->width);
                assert(static_cast<std::size_t>(height) == pointcloud->height);
                for (std::size_t x = 0; x < static_cast<std::size_t>(width); x += pointSkip)
                {
                    for (std::size_t y = 0; y < static_cast<std::size_t>(height); y += pointSkip)
                    {
                        const auto& pclPoint = cloudTransformedPtr->at(x, y);

                        //transform to point in world coord
                        const Eigen::Vector4f pointFromPcl
                        {
                            pclPoint.x, pclPoint.y, pclPoint.z, 1
                        };

                        const Eigen::Vector4f pointInWorld = transformCamToWorld * pointFromPcl ;
                        const Eigen::Vector4f pointInWorldNormalizedW = pointInWorld / pointInWorld(3);

                        DebugDrawer24BitColoredPointCloudElement dbgPoint;

                        dbgPoint.x = pointInWorldNormalizedW(0);
                        dbgPoint.y = pointInWorldNormalizedW(1);
                        dbgPoint.z = pointInWorldNormalizedW(2);

                        //clip to space
                        if (
                            clipPointCloud &&
                            (
                                dbgPoint.x < clipDrawnCloudXLo || dbgPoint.x > clipDrawnCloudXHi ||
                                dbgPoint.y < clipDrawnCloudYLo || dbgPoint.y > clipDrawnCloudYHi ||
                                dbgPoint.z < clipDrawnCloudZLo || dbgPoint.z > clipDrawnCloudZHi
                            )
                        )
                        {
                            continue;
                        }

                        dbgPoint.color.r = pclPoint.r;
                        dbgPoint.color.g = pclPoint.g;
                        dbgPoint.color.b = pclPoint.b;

                        dbgPcl.points.emplace_back(dbgPoint);
                    }
                }

                debugDrawer->set24BitColoredPointCloudVisu("PointClouds", getName() + "::PointCloud", dbgPcl);
                //                debugDrawer->setPoseVisu("PointClouds", getName() + "::PointCloudBaseNode", new Pose { (pointCloudBaseNode ? pointCloudBaseNode->getGlobalPose() : Eigen::Matrix4f::Identity()) });
            }
        }
        return succeeded;
    }

    bool DepthImageProviderDynamicSimulation::render()
    {
        ARMARX_TRACE;
        //render images
        if (!cameraNode)
        {
            ARMARX_ERROR << deactivateSpam() << "No camera node:" << camNodeName;
            return false;
        }

        bool renderOK = false;
        {
            auto l = simVisu->getScopedLock();
            auto start = IceUtil::Time::now();

            ARMARX_TRACE;
            renderOK = VirtualRobot::CoinVisualizationFactory::renderOffscreenRgbDepthPointcloud(offscreenRenderer.get(), cameraNode, visualization, width, height,
                       true, rgbBuffer,
                       true, depthBuffer,
                       true,
                       pointCloudBuffer, zNear, zFar, vertFov, nanValue);

            ARMARX_DEBUG << deactivateSpam(1) << "Offscreen rendering took: " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
        }
        if (renderOK)
        {
            ARMARX_TRACE;
            //get images + cloud
            std::unique_lock lock(captureMutex);

            auto start = std::chrono::high_resolution_clock::now();

            auto randValuesSize = randValues.size();
            int noiseOffset = noise > 0 ? rand() % randValuesSize : 0;

            float* depthBufferEndOfRow = depthBuffer.data() + width - 1;
            if (floatImageMode)
            {
                float* dFloatImageRow = dFloatImage->pixels;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        float value = depthBufferEndOfRow[-x];
                        value = std::max(value, 0.0f);
                        value = std::min(value, static_cast<float>(0xffffff));
                        dFloatImageRow[x] = value;
                    }
                    dFloatImageRow += width;
                    depthBufferEndOfRow += width;
                }
            }
            else
            {
                ARMARX_TRACE;
                std::uint8_t* rgbImageRow = rgbImage->pixels;
                std::uint8_t* rgbBufferEndOfRow = rgbBuffer.data() + (width - 1) * 3;
                std::uint8_t* dImageRow = dImage->pixels;
                for (int y = 0; y < height; ++y)
                {
                    for (int x = 0; x < width; ++x)
                    {
                        memcpy(
                            static_cast<void*>(&(rgbImageRow[x * 3])), //dest
                            static_cast<const void*>(&(rgbBufferEndOfRow[-x * 3])), //src
                            3 //count
                        );
                        float valueF = depthBufferEndOfRow[-x];
                        valueF = std::max(0.0f, valueF);
                        valueF = std::min(valueF, static_cast<float>(0xffffff));
                        std::uint32_t value = static_cast<std::uint32_t>(valueF);
                        memcpy(
                            static_cast<void*>(&(dImageRow[x * 3])), //dest
                            static_cast<const void*>(&value), //src
                            3 //count
                        );
                    }
                    rgbImageRow += width * 3;
                    rgbBufferEndOfRow += width * 3;
                    dImageRow += width * 3;
                    depthBufferEndOfRow += width;
                }
            }

            ARMARX_TRACE;
            float* randValuesData = randValues.data();
            std::uint8_t* rgbBufferEntry = rgbBuffer.data();
            Eigen::Vector3f* inputRow = pointCloudBuffer.data();
            PointT* outputEndOfRow = pointcloud->points.data() + (width - 1);
            for (int y = 0; y < height; ++y)
            {
                ARMARX_TRACE;
                for (int x = 0; x < width; ++x)
                {
                    PointT& point = outputEndOfRow[-x];
                    Eigen::Vector3f pointCloudBufferPoint = inputRow[x];

                    int noiseIndex = (y * width + x) + noiseOffset;
                    float noiseX = noise > 0.0f ? randValuesData[(noiseIndex) % randValuesSize] : 0.0f;
                    float noiseY = noise > 0.0f ? randValuesData[(noiseIndex + 1) % randValuesSize] : 0.0f;
                    float noiseZ = noise > 0.0f ? randValuesData[(noiseIndex + 2) % randValuesSize] : 0.0f;

                    point.x = pointCloudBufferPoint[0] + noiseX;
                    point.y = pointCloudBufferPoint[1] + noiseY;
                    point.z = pointCloudBufferPoint[2] + noiseZ;
                    point.r = rgbBufferEntry[0];
                    point.g = rgbBufferEntry[1];
                    point.b = rgbBufferEntry[2];

                    rgbBufferEntry += 3;
                }
                inputRow += width;
                outputEndOfRow += width;
            }

            auto end = std::chrono::high_resolution_clock::now();
            std::int64_t timeDiff = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
            sumRenderTimes += timeDiff;
            sumRenderTimesSquared += timeDiff * timeDiff;
            renderTimesCount += 1;

            if (renderTimesCount > 256)
            {
                ARMARX_TRACE;
                double averageTime = sumRenderTimes * 1.0e-3 / renderTimesCount;
                double averageSquaredTime = sumRenderTimesSquared * 1.0e-6 / renderTimesCount;
                double stddevTime = std::sqrt(averageSquaredTime - averageTime * averageTime);

                ARMARX_DEBUG << "Copying depth buffer data took: " << averageTime << "ms +- " << stddevTime << "ms";

                renderTimesCount = 0;
                sumRenderTimes = 0;
                sumRenderTimesSquared = 0;
                renderTimesCount = 0;
            }
        }
        return renderOK;
    }
    void DepthImageProviderDynamicSimulation::syncVisu()
    {
        ARMARX_TRACE;
        if (simVisu)
        {
            ARMARX_TRACE;
            simVisu->synchronizeVisualizationData();
            if (simVisu->getRobot(robotName))
            {
                ARMARX_TRACE;
                cameraNode = simVisu->getRobot(robotName)->getRobotNode(camNodeName);
            }
            visualization = simVisu->getVisualization();
            if (!cameraNode)
            {
                ARMARX_WARNING << "The camera node is not in the scene";
            }
            if (!visualization)
            {
                ARMARX_WARNING << "No physics visualization scene";
            }
        }
    }
}
