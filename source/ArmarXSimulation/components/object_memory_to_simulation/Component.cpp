/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::object_memory_to_simulation
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include "ArmarXCore/core/PackagePath.h"
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/libraries/core/Pose.h"

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>


namespace armarx::simulation::components::object_memory_to_simulation
{

    const std::string Component::defaultName = "ObjectMemoryToSimulation";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        def->component(simulator);


        // Add a required property. (The component won't start without a value being set.)
        def->optional(properties.objectPoseProviderName, "p.objectPoseProviderName");

        // Add an optionalproperty.
        // def->optional(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        // def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */

        synchronizeSimulator();
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    inline armarx::PackagePath toPackagePath(const armarx::PackageFileLocation& packageFileLocation)
    {
        return {packageFileLocation.package, packageFileLocation.relativePath};
    }

    void Component::synchronizeSimulator(const ::Ice::Current&)
    {
        ARMARX_INFO << "Synchronizing memory and simulator";

        // TODO enable list for provider
        const std::vector<armarx::objpose::ObjectPose> objectPoses = (properties.objectPoseProviderName.empty()) ? getObjectPoses() : getObjectPosesByProvider(properties.objectPoseProviderName); 

        ARMARX_INFO << "Found " << objectPoses.size() << " objects";

        for (const auto& objectPose : objectPoses)
        {
            ARMARX_INFO << objectPose.objectID.str();

            ObjectFinder finder;
            const auto objectInfo = finder.findObject(objectPose);
            ARMARX_CHECK(objectInfo.has_value());

            const auto globalPoseIce = armarx::toIce(objectPose.objectPoseGlobal);
            const auto instanceName = objectPose.objectID.str();

            const auto articulatedModelFile = objectInfo->getArticulatedModel();
            if (articulatedModelFile.has_value())
            {
                ARMARX_INFO << "Found robot: " << articulatedModelFile->absolutePath;
                // only works on the same machine
                const auto robotName = simulator->addRobotFromFile(toPackagePath(articulatedModelFile.value()).serialize());

                simulator->setRobotPose(robotName, globalPoseIce);
                // simulator->setRobotJointAngles(objectPose.objectJointValues);
            }

            else
            {
                ARMARX_INFO << "Found object: " << toPackagePath(objectInfo->simoxXML()).toSystemPath();
                const armarx::PackagePath packagePath(toPackagePath(objectInfo->simoxXML()));
                simulator->addObjectFromFile(
                    packagePath.serialize(), instanceName, globalPoseIce, objectPose.isStatic);
            }
        }
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    // FIXME enable after migration ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::simulation::components::object_memory_to_simulation
