/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::units
 * @author     Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotPoseUnit.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <IceUtil/Time.h>

#include <string>
#include <mutex>
#include <cmath>

namespace armarx
{
    /**
     * @class RobotPoseUnitDynamicSimulationPropertyDefinitions
     * @brief Moves the robot around. Currently only position control is implemented.
     */
    class RobotPoseUnitDynamicSimulationPropertyDefinitions :
        public RobotPoseUnitPropertyDefinitions
    {
    public:
        RobotPoseUnitDynamicSimulationPropertyDefinitions(std::string prefix) :
            RobotPoseUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("IntervalMs", 100, "The time in milliseconds between two calls to the simulation method.");

            //assures that the RobotPose still gets some amount of velocity when near target
            //defineOptionalProperty<float>("LinearVelocityMin", 0.5, "Minimum Linear velocity of the RobotPose (unit: m/sec).");
            //defineOptionalProperty<float>("LinearVelocityMax", 1.5, "Maximum Linear velocity of the RobotPose (unit: m/sec).");
            //defineOptionalProperty<float>("LinearAccelerationMax", 0.9, "Maximum Linear Acceleration (unit: m/sec^2");
            //defineOptionalProperty<float>("AngularVelocityMax", 0.8, "Maximum Angular velocity of the RobotPose (unit: rad/sec).");
            //defineOptionalProperty<float>("AngularAccelerationMax", 1.9, "Maximum Angular Acceleration (unit: rad/sec^2");
            defineOptionalProperty<std::string>("RobotName", "Armar3", "Name of the Robot to use.");
            //defineOptionalProperty<std::string>("RobotRootNodeName", "Platform", "Name of the Robot node that is used as root. This node needs to have a model attached.");
            defineOptionalProperty<std::string>("SimulatorProxyName", "Simulator", "Name of the simulator proxy to use.");
        }
    };

    /**
     * @class RobotPoseUnitDynamicSimulation
     * @brief This unit connects to the physics simulator topic (default: "Simulator") and handles RobotPose movements.
     *
     * @ingroup SensorActorUnits
     * @ingroup ArmarXSimulatorComponents
     */
    class RobotPoseUnitDynamicSimulation :
        virtual public RobotPoseUnit,
        virtual public RobotPoseUnitDynamicSimulationInterface
    {
    public:

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "RobotPoseUnit";
        }

        void onInitRobotPoseUnit() override;
        void onStartRobotPoseUnit() override;
        void onStopRobotPoseUnit() override;
        void onExitRobotPoseUnit() override;

        void simulationFunction();

        // proxy implementation

        void moveTo(const PoseBasePtr& targetPose, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;

        void move(const PoseBasePtr& targetVelocity, const Ice::Current& c = Ice::emptyCurrent) override;

        void moveRelative(const PoseBasePtr& relativeTarget, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;

        void setMaxVelocities(Ice::Float positionalVelocity, Ice::Float orientationalVelocity, const Ice::Current& c = Ice::emptyCurrent) override;

        void stopMovement(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void updateCurrentPose(const PoseBasePtr& newPose);
        //void updateCurrentVelocity(const Vector3BasePtr& translationVel, const Vector3BasePtr& rotationVel);

        // implement SimulatorRobotListener to receive robot updates from simulator
        void reportState(SimulatedRobotState const& state, const Ice::Current&) override;

        void setCurrentPose(const PoseBasePtr& newPose);


        Eigen::Vector3f getRPY(PoseBasePtr pose);

        std::mutex currentPoseMutex;
        IceUtil::Time lastExecutionTime;
        int intervalMs;


        Pose currentPose;
        Pose targetPose;
        ::Ice::Float positionalAccuracy;
        ::Ice::Float orientationalAccuracy;

        std::string simulatorPrxName;
        std::string robotName;
        ControlMode RobotPoseControlMode;

        SimulatorInterfacePrx simulatorPrx;

        PeriodicTask<RobotPoseUnitDynamicSimulation>::pointer_type simulationTask;
    };
}

