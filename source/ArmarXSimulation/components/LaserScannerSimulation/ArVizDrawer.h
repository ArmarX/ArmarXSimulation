/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <set>

#include <VirtualRobot/BoundingBox.h>

#include <RobotAPI/components/ArViz/Client/ScopedClient.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>

#include "ArmarXSimulation/components/LaserScannerSimulation/OccupancyGrid.h"
#include "RobotAPI/components/ArViz/Client/Elements.h"

namespace armarx::laser_scanner_simulation
{
    class ArVizDrawer : virtual public armarx::viz::ScopedClient
    {
    public:
        using ScopedClient::ScopedClient;

        void drawBoundingBoxes(const std::vector<VirtualRobot::BoundingBox>& boundingBoxes);
        void drawOccupancyGrid(const OccupancyGrid& grid, float boxPosZ);

        void prepareScan(const LaserScan& scan, const std::string& frame, const Eigen::Affine3f& scannerPose);
        void drawScans();

    private:
        // as the set is ordered, laser scans will always have the same color
        std::set<std::string> frames;

        std::unordered_map<std::string, viz::PointCloud> clouds;
    };

} // namespace armarx::laser_scanner_simulation
