/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author     Nikolaus Vahrenkamp
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/services/sharedmemory/SharedMemoryConsumer.h>
#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/interface/components/Calibration.h>

#include <string>
#include <mutex>

// IVT
#include <Image/ByteImage.h>
#include <Calibration/StereoCalibration.h>

#include <VirtualRobot/Robot.h>
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <ArmarXSimulation/components/ArmarXPhysicsWorldVisualization/ArmarXPhysicsWorldVisualization.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

namespace armarx
{
    class ImageSequenceProviderDynamicSimulationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ImageSequenceProviderDynamicSimulationPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("FrameRate", 30.0f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(60.0f);
            defineOptionalProperty<visionx::ImageDimension>("ImageSize", visionx::ImageDimension(640,  480), "Target resolution of the images. Captured images will be converted to this size.")
            .setCaseInsensitive(true)
            .map("320x240",     visionx::ImageDimension(320,  240))
            .map("640x480",     visionx::ImageDimension(640,  480))
            .map("800x600",     visionx::ImageDimension(800,  600))
            .map("768x576",     visionx::ImageDimension(768,  576))
            .map("1024x768",    visionx::ImageDimension(1024, 768))
            .map("1280x960",    visionx::ImageDimension(1280, 960))
            .map("1600x1200",   visionx::ImageDimension(1600, 1200))
            .map("none",        visionx::ImageDimension(0, 0));
            //defineOptionalProperty<std::string>("SimulatorControlName", "ArmarXSimulatorControl", "Name of the simulator control component that should be used");
            defineOptionalProperty<std::string>("RobotName", "Armar3", "The robot");
            defineOptionalProperty<std::string>("RobotNodeLeftCamera", "EyeLeftCameraSim", "The coordinate system of the left camera");
            defineOptionalProperty<std::string>("RobotNodeRightCamera", "EyeRightCameraSim", "The coordinate system of the right camera");
            defineOptionalProperty<std::string>("CalibrationFile", "", "Camera calibration file, will be made available in the SLICE interface");
            defineOptionalProperty<std::string>("ReferenceFrame", "EyeLeftCameraSim", "Reference frame in the robot model of this stereo camera system.");
        }
    };

    /**
     * Image provider captures images from the simulator and broadcasts
     * notifications at a specified frame rate.
     *
     * It supports the following image transmission formats:
     *
     *  - RGB
     *  - Gray Scale
     *
     */
    class ImageProviderDynamicSimulation :
        virtual public visionx::CapturingImageProvider,
        virtual public visionx::StereoCalibrationCaptureProviderInterface
    {
    public:
        std::string getDefaultName() const override
        {
            return "DynamicSimulationImageProvider";
        }


    protected:
        /**
         * @see visionx::CapturingImageProvider::onInitCapturingImageProvider()
         */
        void onInitCapturingImageProvider() override;

        /**
         * @see visionx::CapturingImageProvider::onExitCapturingImageProvider()
         */
        void onExitCapturingImageProvider() override;

        /**
         * @see visionx::CapturingImageProvider::onStartCapture(float frameRate)
         */
        void onStartCapture(float frameRate) override;

        /**
         * @see visionx::CapturingImageProvider::onStopCapture()
         */
        void onStopCapture() override;

        bool capture(void** ppImageBuffers) override;


        /**
         * Returns the StereoCalibration as provided in configuration
         *
         * @return visionx::StereoCalibration
         */
        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Returns whether images are undistorted
         *
         * @return bool
         */
        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override;
        std::string getReferenceFrame(const ::Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new ImageSequenceProviderDynamicSimulationPropertyDefinitions(
                           getConfigIdentifier()));
        }
        CByteImage**             images;
        CByteImage**             resizedImages;

        std::mutex             captureMutex;

        //VirtualRobot::RobotNodePtr leftCameraNode;
        //VirtualRobot::RobotNodePtr rightCameraNode;

        int renderImg_bpp;
        int renderImg_width;
        int renderImg_height;
        float fovLeft, fovRight;

        //RemoteRobotPtr remoteRobot;

        ArmarXPhysicsWorldVisualizationPtr simVisu;

        //SimulatorControlInterfacePrx simulatorProxy;
        /*std::string memoryNameLeft;
        std::string memoryNameRight;
        SharedMemoryConsumer<unsigned char>::pointer_type sharedMemoryConsumerLeft;
        SharedMemoryConsumer<unsigned char>::pointer_type sharedMemoryConsumerRight;*/

        std::shared_ptr<SoOffscreenRenderer> rendererLeft;
        std::shared_ptr<SoOffscreenRenderer> rendererRight;

        std::string robotName;
        std::string leftNodeName;
        std::string rightNodeName;
        VirtualRobot::RobotNodePtr cameraNodeL;
        VirtualRobot::RobotNodePtr cameraNodeR;

        CStereoCalibration ivtStereoCalibration;
        visionx::StereoCalibration stereoCalibration;

        bool setupCameraRendering(const std::string& robotName, const std::string& cameraSensorNameLeft, const std::string& cameraSensorNameRight);
        bool updateCameraRendering();
    private:

    };

}


