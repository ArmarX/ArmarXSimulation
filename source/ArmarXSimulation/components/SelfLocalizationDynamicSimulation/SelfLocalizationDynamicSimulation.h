/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation
 * @author     Nikolaus Vahrenkamp
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// Simulator
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <RobotComponents/libraries/SelfLocalization/SelfLocalization.h>

namespace armarx
{
    /**
     * SelfLocalizationDynamicSimulation uses the armarx simulator to retrieve thre current psoe of the robot and reports it to the agent layer.
     */
    class SelfLocalizationDynamicSimulation:
        virtual public SelfLocalization

    {
    public:

        SelfLocalizationDynamicSimulation() = default;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onConnectComponent() override;
        void onDisconnectComponent() override;

        Eigen::Affine3f worldToMapTransform() const override
        {
            return Eigen::Affine3f::Identity();
        }

        /**
        * @see Component::getDefaultName()
        *
        */
        std::string getDefaultName() const override
        {
            return "SelfLocalizationDynamicSimulation";
        }

    protected:

        PeriodicTask<SelfLocalizationDynamicSimulation>::pointer_type execTask;

    private:

        void periodicExec();

        void reportRobotPose();

        SimulatorInterfacePrx simulatorProxy;

        std::mutex dataMutex;

        struct Properties
        {
            int cycleTime = 30;
            std::string robotName;
        } p;

    };

}  // namespace armarx

