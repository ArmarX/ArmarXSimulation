/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation
 * @author     Nikolaus Vahrenkamp
 * @author     Fabian Reister
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelfLocalizationDynamicSimulation.h"

// Robot API
#include <IceUtil/Time.h>
#include "ArmarXCore/core/time/Clock.h"
#include <RobotAPI/libraries/core/Pose.h>

namespace armarx
{

    armarx::PropertyDefinitionsPtr SelfLocalizationDynamicSimulation::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = SelfLocalization::createPropertyDefinitions();

        defs->component(simulatorProxy);

        defs->optional(p.cycleTime, "cycleTime");
        defs->required(p.robotName, "RobotName");

        return defs;
    }

    void SelfLocalizationDynamicSimulation::onConnectComponent()
    {
        SelfLocalization::onConnectComponent();

        ARMARX_CHECK(simulatorProxy->hasRobot(p.robotName))
                << "Robot with given name not available in armarx simulator: " + p.robotName;

        ARMARX_INFO << "Using robot with name " << p.robotName;

        // periodic task setup
        if (execTask)
        {
            execTask->stop();
        }

        execTask = new PeriodicTask<SelfLocalizationDynamicSimulation>(
            this,
            &SelfLocalizationDynamicSimulation::reportRobotPose,
            p.cycleTime,
            false,
            "SelfLocalizationDynamicSimulationCalculation");
        execTask->start();
        execTask->setDelayWarningTolerance(100);
    }

    void SelfLocalizationDynamicSimulation::onDisconnectComponent()
    {
        SelfLocalization::onDisconnectComponent();

        ARMARX_INFO << "Disconnect...";
        if (execTask)
        {
            execTask->stop();
        }
    }

    void SelfLocalizationDynamicSimulation::reportRobotPose()
    {
        try
        {
            std::unique_lock lock(dataMutex);

            const Eigen::Affine3f globalPose = [&]() -> Eigen::Affine3f
            {
                const PoseBasePtr poseBase = simulatorProxy->getRobotPose(p.robotName);

                if (not poseBase)
                {
                    ARMARX_WARNING << deactivateSpam(10) << "No robot pose from simulator...";
                    Eigen::Affine3f::Identity();
                }

                return Eigen::Affine3f(PosePtr::dynamicCast(poseBase)->toEigen());
            }();

            // world and map are identical
            const PoseStamped mapPose{.pose = globalPose, .timestamp = Clock::Now()};

            TimeUtil::SleepMS(50);

            publishSelfLocalization(mapPose);

            // ARMARX_INFO << deactivateSpam(0.1) << globalPose.translation();
        }
        catch (...)
        {
            ARMARX_WARNING << deactivateSpam(10)
                           << "Could not update robot pose in agents segment...\n"
                           << GetHandledExceptionString();
        }
    }

} // namespace armarx
