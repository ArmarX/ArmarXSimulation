/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "PlatformSimulationDevice.h"

armarx::PlatformSimulationDevice::PlatformSimulationDevice(const std::string& name):
    DeviceBase(name), SensorDevice(name), SensorDeviceTemplate(name), ControlDevice(name)
{
    jointVel.target = &target;
    jointStopMovCtrl.target = &target;
    jointEmergencyCtrl.target = &target;
    addJointController(&jointVel);
    addJointController(&jointStopMovCtrl);
    addJointController(&jointEmergencyCtrl);
}

void armarx::PlatformSimulationDevice::JointEmergencyStopController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    target->velocityRotation = 0;
    target->velocityX = 0;
    target->velocityY = 0;
}

void armarx::PlatformSimulationDevice::JointStopMovementController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    target->velocityRotation = 0;
    target->velocityX = 0;
    target->velocityY = 0;
}
