/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "JointSimulationDevice.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

const float armarx::JointSimulationDevice::nullFloat = 0;

armarx::JointSimulationDevice::JointSimulationDevice(const std::string& name, armarx::NameValueMap& ctrlpos, armarx::NameValueMap& ctrlvel, armarx::NameValueMap& ctrltor):
    DeviceBase(name), SensorDevice(name), SensorDeviceTemplate(name), ControlDevice(name)
{
    jointCtrlPos.map = &ctrlpos;
    jointCtrlVel.map = &ctrlvel;
    jointCtrlTor.map = &ctrltor;
    jointCtrlESt.map = &ctrlvel;
    jointCtrlMSt.map = &ctrlvel;

    jointCtrlPos.val = &jointCtrlPos.targ.position;
    jointCtrlVel.val = &jointCtrlVel.targ.velocity;
    jointCtrlTor.val = &jointCtrlTor.targ.torque;
    jointCtrlESt.val = &nullFloat;
    jointCtrlMSt.val = &nullFloat;

    jointCtrlPos.sensVal = &(sensorValue.position);
    jointCtrlVel.sensVal = &(sensorValue.velocity);
    jointCtrlTor.sensVal = &(sensorValue.torque);
    jointCtrlESt.sensVal = &nullFloat;
    jointCtrlMSt.sensVal = &nullFloat;

    addJointController(&jointCtrlPos);
    addJointController(&jointCtrlVel);
    addJointController(&jointCtrlTor);
    addJointController(&jointCtrlESt);
    addJointController(&jointCtrlMSt);
}

void armarx::JointSimulationDevice::rtSetActiveJointController(armarx::JointController* jointCtrl)
{
    if (rtGetActiveJointController())
    {
        auto curJointCtrl = dynamic_cast<JointSimControllerBase*>(rtGetActiveJointController());
        curJointCtrl->map->erase(getDeviceName());
        curJointCtrl->mapVal = nullptr;
    }
    ControlDevice::rtSetActiveJointController(jointCtrl);
    auto newJointCtrl = dynamic_cast<JointSimControllerBase*>(jointCtrl);
    newJointCtrl->mapVal = &(*(newJointCtrl->map))[getDeviceName()];
    *(newJointCtrl->mapVal) = *(newJointCtrl->sensVal);
}

void armarx::JointSimulationDevice::JointSimControllerBase::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    ARMARX_CHECK_EXPRESSION(mapVal) << "JointBaseCtrl(" + getControlMode() + ") has null map target value";
    ARMARX_CHECK_EXPRESSION(val) << "JointBaseCtrl(" + getControlMode() + ") has null target value";
    *mapVal = *val;
}
