/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/observers/filters/ButterworthFilter.h>

#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueForceTorque.h>

namespace armarx
{
    TYPEDEF_PTRS_SHARED(ForceTorqueSimulationSensorDevice);
    class ForceTorqueSimulationSensorDevice: virtual public SensorDeviceTemplate<SensorValueForceTorque>
    {
    public:
        ForceTorqueSimulationSensorDevice(
            const std::string& name,
            const std::string reportingFrame,
            const Eigen::Matrix3f reportingTransformation
        ):
            DeviceBase(name),
            SensorDevice(name),
            SensorDeviceTemplate(name),
            reportingFrame {reportingFrame},
            reportingTransformation {reportingTransformation}
        {}

        std::string getReportingFrame() const override
        {
            return reportingFrame;
        }

        filters::ButterworthFilter tx {2, 100, Lowpass, 1};
        filters::ButterworthFilter ty {2, 100, Lowpass, 1};
        filters::ButterworthFilter tz {2, 100, Lowpass, 1};
        filters::ButterworthFilter fx {2, 100, Lowpass, 1};
        filters::ButterworthFilter fy {2, 100, Lowpass, 1};
        filters::ButterworthFilter fz {2, 100, Lowpass, 1};

        const std::string reportingFrame;
        const Eigen::Matrix3f reportingTransformation;
    };
}

