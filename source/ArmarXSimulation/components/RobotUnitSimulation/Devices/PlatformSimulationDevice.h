/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/ControlModes.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueHolonomicPlatform.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>

namespace armarx
{
    TYPEDEF_PTRS_SHARED(PlatformSimulationDevice);
    class PlatformSimulationDevice: virtual public SensorDeviceTemplate<SensorValueHolonomicPlatform>, virtual public ControlDevice
    {
    public:
        struct JointVelocityController : public JointController
        {
            ControlTargetHolonomicPlatformVelocity* target;
            void rtRun(const IceUtil::Time&, const IceUtil::Time&) override {}
            ControlTargetBase* getControlTarget() override
            {
                return target;
            }
            std::string getHardwareControlMode() const override
            {
                return "Platform_Velocity";
            }
        };

        struct JointEmergencyStopController : public JointController
        {
            ControlTargetHolonomicPlatformVelocity* target;
            void rtRun(const IceUtil::Time&, const IceUtil::Time&) override;
            ControlTargetBase* getControlTarget() override
            {
                return target;
            }
            const std::string& getControlMode() const override
            {
                return ControlModes::EmergencyStop;
            }
            bool rtIsTargetValid() const override
            {
                return true;
            }
            std::string getHardwareControlMode() const override
            {
                return "Platform_Velocity";
            }
        };

        struct JointStopMovementController : public JointController
        {
            ControlTargetHolonomicPlatformVelocity* target;
            void rtRun(const IceUtil::Time&, const IceUtil::Time&) override;
            ControlTargetBase* getControlTarget() override
            {
                return target;
            }
            const std::string& getControlMode() const override
            {
                return ControlModes::StopMovement;
            }
            bool rtIsTargetValid() const override
            {
                return true;
            }
            std::string getHardwareControlMode() const override
            {
                return "Platform_Velocity";
            }
        };


        PlatformSimulationDevice(const std::string& name);

        ControlTargetHolonomicPlatformVelocity target;

        float initAbsolutePositionX;
        float initAbsolutePositionY;
        float initAbsolutePositionRotation;

        JointVelocityController jointVel;
        JointEmergencyStopController jointEmergencyCtrl;
        JointStopMovementController jointStopMovCtrl;
    };
}
