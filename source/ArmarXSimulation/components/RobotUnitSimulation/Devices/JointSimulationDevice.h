/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/ControlModes.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

namespace armarx
{
    TYPEDEF_PTRS_SHARED(JointSimulationDevice);
    class JointSimulationDevice: virtual public SensorDeviceTemplate<SensorValue1DoFActuator>, virtual public ControlDevice
    {
        static const float nullFloat;
    public:
        struct JointSimControllerBase : public JointController
        {
            NameValueMap* map;
            const float* val;
            const float* sensVal;
            float* mapVal;
            void rtRun(const IceUtil::Time&, const IceUtil::Time&) override;
        };
        template<class TargT>
        struct JointSimController : public JointSimControllerBase
        {
            JointSimController(std::string hwMode) : hwMode {hwMode} {}

            TargT targ;
            const std::string hwMode;
            ControlTargetBase* getControlTarget() override
            {
                return &targ;
            }
            std::string getHardwareControlMode() const override
            {
                return hwMode;
            }
        };

        JointSimulationDevice(const std::string& name, NameValueMap& ctrlpos, NameValueMap& ctrlvel, NameValueMap& ctrltor);
        void rtSetActiveJointController(JointController* jointCtrl) override;

        JointSimController<ControlTarget1DoFActuatorPosition> jointCtrlPos {"Position"};
        JointSimController<ControlTarget1DoFActuatorVelocity> jointCtrlVel {"Velocity"};
        JointSimController<ControlTarget1DoFActuatorTorque> jointCtrlTor {"Torque"};
        JointSimController<DummyControlTargetEmergencyStop> jointCtrlESt {"Velocity"};
        JointSimController<DummyControlTargetStopMovement> jointCtrlMSt {"Velocity"};
    };
}

