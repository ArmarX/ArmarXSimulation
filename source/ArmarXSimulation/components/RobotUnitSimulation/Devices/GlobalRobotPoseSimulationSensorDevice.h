/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <RobotAPI/components/units/RobotUnit/Devices/GlobalRobotPoseSensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueBase.h>

#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx
{
    

    TYPEDEF_PTRS_SHARED(GlobalRobotPoseSimulationSensorDevice);
    class GlobalRobotPoseSimulationSensorDevice: virtual public SensorDevice
    {
    public:
        static std::string DeviceName()
        {
            return "GlobalRobotPoseSimulationSensorDevice";
        }
        GlobalRobotPoseSimulationSensorDevice(): DeviceBase(DeviceName()), SensorDevice(DeviceName()) {}

        const SensorValueBase* getSensorValue() const override
        {
            return &sensor;
        }

        std::string getReportingFrame() const override
        {
            return GlobalFrame;
        }

        SensorValueGlobalRobotPose sensor;
    };
}
