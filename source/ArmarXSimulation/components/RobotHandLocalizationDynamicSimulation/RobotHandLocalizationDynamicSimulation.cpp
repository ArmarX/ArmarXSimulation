/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotHandLocalizationDynamicSimulation.h"

// Core
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// MemoryX
#include <MemoryX/core/entity/Entity.h>

using namespace armarx;
using namespace memoryx;
using namespace memoryx::EntityWrappers;


RobotHandLocalizationDynamicSimulation::RobotHandLocalizationDynamicSimulation()
    = default;


void RobotHandLocalizationDynamicSimulation::onInitComponent()
{
    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    tcpNameLeft = getProperty<std::string>("TCPNameLeft").getValue();
    tcpNameRight = getProperty<std::string>("TCPNameRight").getValue();
    referenceFrame = getProperty<std::string>("ReferenceFrameName").getValue();

    std::string simPrxName = getProperty<std::string>("SimulatorName").getValue();

    if (!simPrxName.empty())
    {
        usingProxy(simPrxName);
    }
}


void RobotHandLocalizationDynamicSimulation::onConnectComponent()
{
    ARMARX_VERBOSE << "onConnect";

    try
    {
        RobotStateComponentInterfacePrx robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));

        std::string simPrxName = getProperty<std::string>("SimulatorName").getValue();

        if (!simPrxName.empty())
        {
            simulatorProxy = getProxy<SimulatorInterfacePrx>(simPrxName);
        }


        // check nodes
        VirtualRobot::RobotNodePtr rn = remoteRobot->getRobotNode(referenceFrame);
        ARMARX_CHECK_EXPRESSION(rn) << "Reference frame not present in remote robot!";
        rn = remoteRobot->getRobotNode(tcpNameLeft);
        ARMARX_CHECK_EXPRESSION(rn) << "tcpNameLeft frame not present in remote robot!";
        rn = remoteRobot->getRobotNode(tcpNameRight);
        ARMARX_CHECK_EXPRESSION(rn) << "tcpNameRight frame not present in remote robot!";
    }
    catch (...)
    {
        handleExceptions();
    }
}

void RobotHandLocalizationDynamicSimulation::onDisconnectComponent()
{
    ARMARX_VERBOSE << "onDisConnect";

    try
    {
        remoteRobot.reset();
        simulatorProxy = nullptr;
    }
    catch (...)
    {
        handleExceptions();
    }
}



bool RobotHandLocalizationDynamicSimulation::addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager)
{
    ARMARX_VERBOSE << "Adding object class " << objectClassEntity->getName() << armarx::flush;

    return true;
}


memoryx::ObjectLocalizationResultList RobotHandLocalizationDynamicSimulation::localizeObjectClasses(const ::memoryx::ObjectClassNameList& objectClassNames, const ::Ice::Current&)
{
    ARMARX_DEBUG << "localizeObjectClasses() started";
    TimestampVariantPtr timestamp = TimestampVariant::nowPtr();
    bool firstEntryIsLeft = objectClassNames.at(0).find("left") != std::string::npos || objectClassNames.at(0).find("Left") != std::string::npos;

    auto agentName = remoteRobot->getName();
    PosePtr rnPoseLeft = simulatorProxy ? PosePtr::dynamicCast(simulatorProxy->getRobotNodePose(agentName, tcpNameLeft)) : new Pose(remoteRobot->getRobotNode(tcpNameLeft)->getGlobalPose());
    PosePtr rnPoseRight = simulatorProxy ? PosePtr::dynamicCast(simulatorProxy->getRobotNodePose(agentName, tcpNameRight)) : new Pose(remoteRobot->getRobotNode(tcpNameRight)->getGlobalPose());
    Eigen::Matrix4f tcpLeftGlobalPose = rnPoseLeft->toEigen();
    Eigen::Matrix4f tcpRightGlobalPose = rnPoseRight->toEigen();
    //Eigen::Matrix4f cameraPose = remoteRobot->getRobotNode(referenceFrame)->getPoseInRootFrame();
    // convert all poses to reference frame/camera coordinates
    Eigen::Matrix4f cameraPose = simulatorProxy ? PosePtr::dynamicCast(simulatorProxy->getRobotNodePose(agentName, referenceFrame))->toEigen() : remoteRobot->getRobotNode(referenceFrame)->getGlobalPose();
    tcpLeftGlobalPose = cameraPose.inverse() * tcpLeftGlobalPose;
    tcpRightGlobalPose = cameraPose.inverse() * tcpRightGlobalPose;

    armarx::FramedPositionPtr positionLeftHand = new armarx::FramedPosition(tcpLeftGlobalPose, referenceFrame, agentName);
    armarx::FramedPositionPtr positionRightHand = new armarx::FramedPosition(tcpRightGlobalPose, referenceFrame, agentName);
    armarx::FramedOrientationPtr orientationLeftHand = new armarx::FramedOrientation(tcpLeftGlobalPose, referenceFrame, agentName);
    armarx::FramedOrientationPtr orientationRightHand = new armarx::FramedOrientation(tcpRightGlobalPose, referenceFrame, agentName);

    // get poses of hands and markers
    /*Eigen::Matrix4f leftHandPose = remoteRobot->getRobotNode(tcpNameLeft)->getPoseInRootFrame();
    Eigen::Matrix4f rightHandPose = remoteRobot->getRobotNode(tcpNameRight)->getPoseInRootFrame();
    Eigen::Matrix4f leftMarkerPose = remoteRobot->getRobotNode(markerNameLeft)->getPoseInRootFrame();
    Eigen::Matrix4f rightMarkerPose = remoteRobot->getRobotNode(markerNameRight)->getPoseInRootFrame();*/

    // return poses from simulator
    memoryx::ObjectLocalizationResult result;
    result.timeStamp = timestamp;

    memoryx::ObjectLocalizationResultList resultList;
    result.recognitionCertainty = 1;

    if (objectClassNames.size() == 1)
    {
        ARMARX_DEBUG << "Returning hand pose from simulator for one hand: " << objectClassNames.at(0);

        result.objectClassName = objectClassNames.at(0);

        if (firstEntryIsLeft)
        {
            result.position = positionLeftHand;
            result.orientation = orientationLeftHand;

            result.positionNoise = computePositionNoise(positionLeftHand->toEigen());
        }
        else
        {
            result.position = positionRightHand;
            result.orientation = orientationRightHand;
            result.positionNoise = computePositionNoise(positionRightHand->toEigen());
        }
        resultList.push_back(result);
    }
    else if (objectClassNames.size() == 2)
    {
        ARMARX_INFO << "Returning hand pose from simulator for two hands: " << objectClassNames.at(0) << ", " << objectClassNames.at(1);

        result.objectClassName =  firstEntryIsLeft ?  objectClassNames.at(0) : objectClassNames.at(1);
        result.position = positionLeftHand;
        result.orientation = orientationLeftHand;
        result.positionNoise = computePositionNoise(positionLeftHand->toEigen());
        resultList.push_back(result);

        result.objectClassName =  firstEntryIsLeft ?  objectClassNames.at(1) : objectClassNames.at(0);
        result.position = positionRightHand;
        result.orientation = orientationRightHand;
        result.positionNoise = computePositionNoise(positionRightHand->toEigen());
        resultList.push_back(result);
    }
    else
    {
        ARMARX_WARNING << "More than 2 hands requested - this case is not handeled here";
    }


    ARMARX_DEBUG << "Finished localization, returning.";

    return resultList;
}


::memoryx::MultivariateNormalDistributionPtr RobotHandLocalizationDynamicSimulation::computePositionNoise(const Eigen::Vector3f& pos)
{
    FloatVector mean;
    mean.push_back(pos(0));
    mean.push_back(pos(1));
    mean.push_back(pos(2));

    // assuming 2 mm noise
    MultivariateNormalDistributionPtr posDist = new MultivariateNormalDistribution(3);
    posDist->setMean(mean);
    posDist->setCovariance(0, 0, 2.0f);
    posDist->setCovariance(1, 1, 2.0f);
    posDist->setCovariance(2, 2, 2.0f);
    return posDist;
}

