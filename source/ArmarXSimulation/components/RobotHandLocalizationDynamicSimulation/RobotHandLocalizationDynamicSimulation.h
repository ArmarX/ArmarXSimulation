/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

// MemoryX
#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/core/GridFileManager.h>
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>

// Simulator
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

namespace armarx
{
    class RobotHandLocalizationDynamicSimulationPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        RobotHandLocalizationDynamicSimulationPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TCPNameLeft", "TCP L", "Name of the TCP of the left robot hand.");
            defineOptionalProperty<std::string>("TCPNameRight", "TCP R", "Name of the TCP of the right robot hand.");
            defineOptionalProperty<std::string>("SimulatorName", "Simulator", "Name of the simulator component that should be used");
            defineOptionalProperty<std::string>("ReferenceFrameName", "EyeLeftCamera", "Sets the reference frame name of the pose provided by this recognizer. Must be a frame name known in Pose from the robot model.");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");

        }
    };


    /**
     * RobotHandLocalizationDynamicSimulation queries the armarx simulator for the current poses of the hand/TCP coordinate systems of the robot.
     * The hand localization is invoked automatically by the working memory if the object has been requested there.
     */
    class RobotHandLocalizationDynamicSimulation:
        virtual public ::armarx::Component,
        virtual public ::memoryx::ObjectLocalizerInterface
    {
    public:

        RobotHandLocalizationDynamicSimulation();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new RobotHandLocalizationDynamicSimulationPropertyDefinitions(getConfigIdentifier()));
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override {}

        /**
        * @see Component::getDefaultName()
        *
        * Use "HandMarkerLocalization" here in order to mimic the execution on the real robot.
        */
        std::string getDefaultName() const override
        {
            return "HandMarkerLocalization";
        }

    protected:


        /**
         * Add a memory entity representing the hand marker in order to set its properties
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity
         */
        virtual bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager);

        /**
         * localize one or both hand markers
         *
         * @param objectClassNames
         *
         * @return list of object instances
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const ::memoryx::ObjectClassNameList&, const ::Ice::Current& = Ice::emptyCurrent) override;


    private:

        ::memoryx::MultivariateNormalDistributionPtr computePositionNoise(const Eigen::Vector3f& pos);


        // remote robot for pose calculation
        std::shared_ptr<RemoteRobot> remoteRobot;
        std::string tcpNameLeft, tcpNameRight;

        SimulatorInterfacePrx simulatorProxy;

        // reference frame name (e.g. a camera frame)
        std::string referenceFrame;

    };
}

