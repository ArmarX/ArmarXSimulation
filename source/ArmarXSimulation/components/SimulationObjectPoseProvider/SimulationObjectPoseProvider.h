/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx
 * @author     Philip Scherer ( ulila@student.kit.edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstdint>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/libraries/ArmarXObjects/ProvidedObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

namespace armarx
{
    class SimulationObjectPoseProvider :
        virtual public armarx::Component,
        virtual public armarx::ObjectPoseProviderPluginUser
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

        // ObjectPoseProvider interface
    public:
        objpose::provider::RequestObjectsOutput
        requestObjects(const objpose::provider::RequestObjectsInput& input,
                       const Ice::Current& /* unused */) override;

        objpose::ProviderInfo getProviderInfo(const Ice::Current& /* unused */) override;

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

    private:
        // Private methods go here.
        void poseProviderTaskRun();

        std::vector<objpose::ProvidedObjectPose> getAllPoses(const armarx::SceneVisuData& sceneData,
                                                             const DateTime& time) const;

        void updateKnownObjects(const armarx::SceneVisuData& sceneData);
        void removeExpiredRequests(const DateTime& time);
        std::vector<objpose::ProvidedObjectPose> getRequestedPoses(const DateTime& time) const;

        objpose::ProvidedObjectPose objectPoseFromVisuData(const armarx::ObjectVisuData& visuData,
                                                           const DateTime& time) const;

    private:
        static const std::string defaultName;

        mutable std::mutex knownObjectsMutex;
        std::map<std::string, armarx::ObjectVisuData> knownObjects;

        mutable std::mutex activeRequestsMutex;
        std::map<std::string, armarx::core::time::DateTime> activeRequests;

        SimulatorInterfacePrx simulatorPrx;

        armarx::SimpleRunningTask<>::pointer_type poseProviderTask;

        struct Properties
        {
            std::int64_t updateFrequency = 10;

            // Comma separated list of object entities and durations if desired.
            bool requestAllEntities = false;
            std::string initiallyRequestedEntities = "";
        };
        Properties properties;
    };
} // namespace armarx
