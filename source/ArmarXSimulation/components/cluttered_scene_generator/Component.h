/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::cluttered_scene_generator
 * @author     Patrick Hegemann ( 335495-patrickhegemann at users dot noreply dot gitlab dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>

#include <ArmarXSimulation/interface/ObjectMemoryToSimulationInterface.h>
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
#include <ArmarXSimulation/libraries/ClutteredSceneGenerator/ClutteredSceneGenerator.h>

#include <ArmarXCore/core/Component.h>

#include <filesystem>

namespace armarx::simulation::components::cluttered_scene_generator
{
    class Component :
        virtual public armarx::Component
        // virtual public armarx::ObjectPoseClientPluginUser
        // , virtual public ArmarXSimulation::components::cluttered_scene_generator::ComponentInterface
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */

    private:
        static const std::string defaultName;

        armarx::SimulatorInterfacePrx simulator_;
        ArmarXSimulation::components::object_memory_to_simulation::ComponentInterfacePrx objectMemoryToSimulation_;

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            struct {
                std::string packageName;
                std::string packagePath;
            } out;

            struct {
                uint nScenes = 1;
                int initialSeed = 0;
                
                uint nObjects = 10;
                float verticalSpacing = 50;
            } gen;
        };
        Properties properties_;



        scene_generation::ObjectSet getObjectClasses();
        
        void generateScene(const scene_generation::ClutteredSceneGenerator::Config& config,
                           int seed);

        // std::vector<armarx::objpose::ObjectPose> getObjectPoses();
        // objects::Scene convertScene(const std::vector<armarx::objpose::ObjectPose>& objectPoses);
        objects::Scene getScene();

        void storeScene(const objects::Scene& scene, const std::filesystem::path& path);

        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

    };

}  // namespace armarx::simulation::components::cluttered_scene_generator
