/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::Simulator
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimulatorTimeServerProxy.h"

using namespace armarx;


SimulatorTimeServerProxy::SimulatorTimeServerProxy(Simulator* sim) : simulator(sim) {}

void SimulatorTimeServerProxy::stop(const Ice::Current&)
{
    simulator->stop();
}

void SimulatorTimeServerProxy::start(const Ice::Current&)
{
    simulator->start();
}

void SimulatorTimeServerProxy::setSpeed(Ice::Float newSpeed, const Ice::Current&)
{
    simulator->setSpeed(newSpeed);
}

Ice::Float SimulatorTimeServerProxy::getSpeed(const Ice::Current&)
{
    return simulator->getSpeed();
}

Ice::Long SimulatorTimeServerProxy::getTime(const Ice::Current&)
{
    return simulator->getTime();
}

void SimulatorTimeServerProxy::step(const Ice::Current&)
{
    simulator->step();
}

Ice::Int SimulatorTimeServerProxy::getStepTimeMS(const Ice::Current&)
{
    return simulator->getStepTimeMS();
}

std::string SimulatorTimeServerProxy::getDefaultName() const
{
    return GLOBAL_TIMESERVER_NAME;
}
