#include "SimRobot.h"


namespace mujoco
{

    SimRobot::SimRobot()
        = default;


    SimRobot::SimRobot(VirtualRobot::RobotPtr robot) :
        robot(robot)
    {}

}
