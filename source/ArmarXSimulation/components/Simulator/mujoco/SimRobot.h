#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <VirtualRobot/XML/mujoco/RobotMjcf.h>


namespace mujoco
{

    /// A robot in simulation.
    class SimRobot
    {
    public:

        SimRobot();
        SimRobot(VirtualRobot::RobotPtr robot);





    public:

        /// The VirtualRobot robot.
        VirtualRobot::RobotPtr robot;

        /// The robot MJCF model.
        VirtualRobot::mujoco::RobotMjcf mjcf { robot };

    };

}
