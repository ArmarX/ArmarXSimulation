#pragma once

namespace armarx
{

    class LengthScaling
    {
    public:

        /// Constructor. Enables scaling.
        LengthScaling();


        /// Set scaling of meter to millimeter to 1000.
        void enable();
        /// Set scaling of meter to millimeter to 1.
        void disable();
        /// Enable or disable length scaling.
        void setEnabled(bool enabled);


        /// Get the scaling factor for: millimeter = meter * factor.
        float meterToMilli() const;
        /// Get the scaling factor for: meter = millimeter * factor.
        float milliToMeter() const;


        /// Scale the given meter value to millimeter.
        template <typename ValueT>
        ValueT meterToMilli(const ValueT& meter) const
        {
            return meter * _meterToMilli;
        }

        /// Scale the given millimeter value to meter.
        template <typename ValueT>
        ValueT milliToMeter(const ValueT& millimeter) const
        {
            return millimeter / _meterToMilli;
        }


    private:

        float _meterToMilli = 1000;

    };

}
