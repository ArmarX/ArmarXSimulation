#pragma once

#include <VirtualRobot/SceneObject.h>

#include <MujocoX/libraries/Simulation/SimEntity.h>


namespace mujoco
{
    class Data;

    struct SimObject
    {
        SimObject();
        SimObject(const std::string& name);
        SimObject(VirtualRobot::SceneObjectPtr sceneObject);

        void update(mujoco::Model& model);
        void updateSceneObjectPose(const Data& data, float lengthScaling = 1.);

        const std::string& name() const;


        mujoco::SimBody body;
        mujoco::SimGeom geom;

        VirtualRobot::SceneObjectPtr sceneObject;
    };

}
