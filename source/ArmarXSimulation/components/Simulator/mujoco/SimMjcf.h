#pragma once

#include <filesystem>

#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/MJCF/Document.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx
{
    class LengthScaling;
}

namespace armarx
{

    class SimMJCF : Logging
    {
    public:

        SimMJCF(mjcf::Document& document, const LengthScaling& scaling,
                const std::string& loggingTag = "SimMJCF");

        void includeBaseFile(const std::string& path, const std::string& relativeFrom);

        void addObjectDefaults(const std::string& className);

        void setTimeStep(float timestep);

        void addFloor(bool enabled, const std::string& texture, const std::string& name, float size);

        void addObject(VirtualRobot::SceneObjectPtr object,
                       VirtualRobot::SceneObject::Physics::SimulationType simType,
                       const std::string& className, const std::filesystem::path& meshDir);


    private:

        mjcf::Document& document;
        const LengthScaling& scaling;

    };

}
