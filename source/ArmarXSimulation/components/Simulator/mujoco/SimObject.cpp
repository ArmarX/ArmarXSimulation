#ifdef MUJOCO_PHYSICS_WORLD
#include "SimObject.h"

#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <MujocoX/libraries/Simulation/Data.h>


using namespace mujoco;


SimObject::SimObject()
{}

SimObject::SimObject(const std::string& name) : body(name), geom(name)
{}

SimObject::SimObject(VirtualRobot::SceneObjectPtr sceneObject) :
    body(sceneObject->getName()), geom(sceneObject->getName()), sceneObject(sceneObject)
{}


void SimObject::update(Model& model)
{
    body.update(model);
    geom.update(model);
}

void SimObject::updateSceneObjectPose(const Data& data, float lengthScaling)
{
    ARMARX_CHECK(sceneObject);
    Eigen::Matrix4f pose = data.getBodyPose(body);
    math::Helpers::Position(pose) *= lengthScaling;
    sceneObject->setGlobalPose(pose);
}

const std::string& SimObject::name() const
{
    return body.name();
}


#endif
