#include "LengthScaling.h"


using namespace armarx;


LengthScaling::LengthScaling()
{
    enable();
}

void LengthScaling::enable()
{
    setEnabled(true);
}

void LengthScaling::disable()
{
    setEnabled(false);
}

void LengthScaling::setEnabled(bool enabled)
{
    _meterToMilli = enabled ? 1000 : 1;
}

float LengthScaling::meterToMilli() const
{
    return _meterToMilli;
}

float LengthScaling::milliToMeter() const
{
    return 1 / _meterToMilli;
}
