/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::Simulator
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/interface/core/TimeServerInterface.h>

#include "Simulator.h"

namespace armarx
{

    class Simulator;
    using SimulatorPtr = IceInternal::Handle<Simulator>;

    /**
     * @brief The SimulatorTimeServerProxy class forwards TimeserverInterface calls to the simulator
     * This is a hack to allow offering the proxies "Simulator" and "MasterTimeServer" at once
     */
    class SimulatorTimeServerProxy:
        virtual public ManagedIceObject,
        virtual public TimeServerInterface
    {
    public:
        SimulatorTimeServerProxy(Simulator* sim);

        // inherited from TimeServerInterface
        void stop(const Ice::Current& c = Ice::emptyCurrent) override;
        void start(const Ice::Current& c = Ice::emptyCurrent) override;
        void setSpeed(Ice::Float newSpeed, const Ice::Current& c = Ice::emptyCurrent) override;
        Ice::Float getSpeed(const Ice::Current& c = Ice::emptyCurrent) override;
        Ice::Long getTime(const Ice::Current& c = Ice::emptyCurrent) override;
        void step(const Ice::Current& c = Ice::emptyCurrent) override;
        Ice::Int getStepTimeMS(const Ice::Current& c = Ice::emptyCurrent) override;

        // inherited from ManagedIceObject
        std::string getDefaultName() const override;
        void onInitComponent() override {}
        void onConnectComponent() override {}
        void onDisconnectComponent() override {}
        void onExitComponent() override {}

    protected:
        SimulatorPtr simulator;
    };

    using SimulatorTimeServerProxyPtr = IceInternal::Handle<SimulatorTimeServerProxy>;

}

