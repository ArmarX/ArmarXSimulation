/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::KinematicSelfLocalization
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "KinematicSelfLocalization.h"

#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

using namespace armarx;

void KinematicSelfLocalization::onInitComponent()
{
    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());

    usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
    usingTopic(getProperty<std::string>("PlatformTopicName").getValue());
}

void KinematicSelfLocalization::onConnectComponent()
{
    robotStateComponentPrx = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    ARMARX_CHECK_EXPRESSION(robotStateComponentPrx);
    ARMARX_INFO << "Retrieved robot state proxy...";

    memoryPrx = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
    ARMARX_CHECK_EXPRESSION(memoryPrx);
    ARMARX_INFO << "Retrieved WM proxy...";

    agentsMemoryPrx = memoryPrx->getAgentInstancesSegment();
    ARMARX_CHECK_EXPRESSION(agentsMemoryPrx);
    ARMARX_INFO << "Retrieved WM agent segment ...";

    Ice::CommunicatorPtr iceCommunicator = getIceManager()->getCommunicator();

    robotName = getProperty<std::string>("RobotName").getValue();
    robotPoseZ = getProperty<float>("RobotPoseZ").isSet() ? getProperty<float>("RobotPoseZ").getValue() : 0;

    std::string agentName = getProperty<std::string>("AgentName").isSet() ? getProperty<std::string>("AgentName").getValue() : robotName;


    ARMARX_INFO << "Creating robot agent with name " << agentName;
    robotAgent = new memoryx::AgentInstance(agentName);
    auto robot = robotStateComponentPrx->getSynchronizedRobot();
    robotAgent->setSharedRobot(robot);
    robotAgent->setName(agentName);
    robotAgent->setStringifiedSharedRobotInterfaceProxy(iceCommunicator->proxyToString(robot));
    robotAgent->setAgentFilePath(robotStateComponentPrx->getRobotFilename());
    ARMARX_INFO << "Creating robot agent...done";

    // Simulating platform movement to initial pose
    PlatformPose pose;
    pose.timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();
    pose.x = getProperty<float>("InitialPlatformPoseX").getValue();
    pose.y = getProperty<float>("InitialPlatformPoseY").getValue();
    pose.rotationAroundZ = getProperty<float>("InitialPlatformPoseAngle").getValue();
    reportPlatformPose(pose);

    // periodic task setup
    cycleTime = 30;

    if (execTask)
    {
        execTask->stop();
    }

    execTask = new PeriodicTask<KinematicSelfLocalization>(this, &KinematicSelfLocalization::reportRobotPose, cycleTime, false, "KinematicSelfLocalizationCalculation");
    execTask->start();
    execTask->setDelayWarningTolerance(100);
}

void KinematicSelfLocalization::onDisconnectComponent()
{
    execTask->stop();
}

void KinematicSelfLocalization::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr KinematicSelfLocalization::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new KinematicSelfLocalizationPropertyDefinitions(
            getConfigIdentifier()));
}

void KinematicSelfLocalization::reportRobotPose()
{
    try
    {
        std::unique_lock lock(dataMutex);
        ARMARX_DEBUG << "Reporting robot agent:" << robotAgent->getName() << ", robotAgentId:" << robotAgentId << ", self:" << this->getName();


        if (!currentRobotPose)
        {
            return;
        }
        robotAgent->setPose(currentRobotPose);

        robotAgentId = agentsMemoryPrx->upsertEntityByName(robotAgent->getName(), robotAgent);
        robotAgent->setId(robotAgentId);


    }
    catch (...)
    {
        ARMARX_WARNING << deactivateSpam(10) << "Could not update robot pose in agents segment...\n" << GetHandledExceptionString();
        robotAgentId.clear();
    }
}

void armarx::KinematicSelfLocalization::reportPlatformPose(PlatformPose const& currentPose, const Ice::Current&)
{
    std::unique_lock lock(dataMutex);
    Eigen::Matrix3f ori;
    ori.setIdentity();
    ori = Eigen::AngleAxisf(currentPose.rotationAroundZ, Eigen::Vector3f::UnitZ());
    Eigen::Vector3f pos;
    pos[0] = currentPose.x;
    pos[1] = currentPose.y;
    pos[2] = robotPoseZ;

    currentRobotPose = new FramedPose(ori, pos, armarx::GlobalFrame, "");
}

void armarx::KinematicSelfLocalization::reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
{
}

void armarx::KinematicSelfLocalization::reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
{
}


void armarx::KinematicSelfLocalization::reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
{
}
