/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::KinematicSelfLocalization
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <RobotAPI/interface/core/RobotState.h>

// MemoryX
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

// Simulator
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

#include <VirtualRobot/VirtualRobot.h>

#include <mutex>

namespace armarx
{
    /**
     * @class KinematicSelfLocalizationPropertyDefinitions
     * @brief
     */
    class KinematicSelfLocalizationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        KinematicSelfLocalizationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlatformTopicName", "PlatformState", "Name of the platform report topic");
            defineRequiredProperty<std::string>("RobotName", "Name of the robot in the simulator (as specified in the robot's xml file)");
            defineOptionalProperty<std::string>("AgentName", "Name of the agent instance. If empty, RobotName property is used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory that should be used");
            defineOptionalProperty<float>("RobotPoseZ", 0, "Set the z component of the robot pose to a fixed value");
            defineOptionalProperty<float>("InitialPlatformPoseX", 0, "Set the x component of the initial platform pose");
            defineOptionalProperty<float>("InitialPlatformPoseY", 0, "Set the y component of the initial platform pose");
            defineOptionalProperty<float>("InitialPlatformPoseAngle", 0, "Set the angle component of the initial platform pose");
        }
    };

    /**
     * @defgroup Component-KinematicSelfLocalization KinematicSelfLocalization
     * @ingroup ArmarXSimulation-Components
     * A description of the component KinematicSelfLocalization.
     *
     * @class KinematicSelfLocalization
     * @ingroup Component-KinematicSelfLocalization
     * @brief Brief description of class KinematicSelfLocalization.
     *
     * Detailed description of class KinematicSelfLocalization.
     */
    class KinematicSelfLocalization :
        virtual public armarx::Component,
        public PlatformUnitListener
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "KinematicSelfLocalization";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    protected:
        void periodicExec();

        void reportRobotPose();
        PeriodicTask<KinematicSelfLocalization>::pointer_type execTask;

        int cycleTime;
        FramedPosePtr currentRobotPose;
        RobotStateComponentInterfacePrx robotStateComponentPrx;

        memoryx::WorkingMemoryInterfacePrx memoryPrx;
        memoryx::AgentInstancesSegmentBasePrx agentsMemoryPrx;
        memoryx::AgentInstancePtr robotAgent;

        std::string robotAgentId;
        std::string robotName;

        std::mutex dataMutex;

        float robotPoseZ;

    public:
        // PlatformUnitListener interface
        void reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& = Ice::emptyCurrent) override;
        void reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current& = Ice::emptyCurrent) override;
        void reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current& = Ice::emptyCurrent) override;
        void reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&) override;
    };
}

