/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::ObjectLocalizerDynamicSimulation
 * @author     welke ( welke at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>
//#include <Calibration/StereoCalibration.h>
//#include <Calibration/Calibration.h>
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>


namespace armarx
{
    class ObjectLocalizerDynamicSimulationPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ObjectLocalizerDynamicSimulationPropertyDefinitions(std::string prefix);
    };

    /**
     * @class ObjectLocalizerDynamicSimulation
     * @brief This component connects to the ArmarX Simulator and reports object poses from the physics world.
     * By default it uses the simulator topic with name "Simulator".
     *
     * @ingroup ArmarXSimulatorComponents
     *
     */
    class ObjectLocalizerDynamicSimulation :
        virtual public armarx::Component,
        virtual public memoryx::ObjectLocalizerInterface
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    protected:

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        /**
         * localizes simulated object instances
         *
         * @param objectClassNames names of the class to localize
         *
         * @return list of object instances
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames, const Ice::Current& c = Ice::emptyCurrent) override;

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        ::memoryx::MultivariateNormalDistributionPtr computePositionNoise(const Eigen::Vector3f& pos);


    private:

        //memoryx::MultivariateNormalDistributionPtr calculateLocalizationUncertainty(Vec2d left_point, Vec2d right_point);
        //memoryx::MultivariateNormalDistributionPtr calculateLocalizationUncertainty(const Eigen::Vector3f& position);
        //void getImagePositions(const Eigen::Vector3f& position, Vec2d& left_point, Vec2d& right_point);
        //bool isVisible(const Eigen::Vector3f& position);

        std::string robotName;
        std::string frameName;
        float recognitionCertainty;
        bool performVisibilityCheck;

        SimulatorInterfacePrx simulatorPrx;
        //CStereoCalibration stereoCalibration;

        // noise
        //Eigen::Vector2d imageNoiseLeft;
        //Eigen::Vector2d imageNoiseRight;
    };
}

