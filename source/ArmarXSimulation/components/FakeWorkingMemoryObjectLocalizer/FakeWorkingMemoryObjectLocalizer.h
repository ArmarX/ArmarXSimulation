/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::FakeWorkingMemoryObjectLocalizer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

namespace armarx
{
    /**
     * @class FakeWorkingMemoryObjectLocalizerPropertyDefinitions
     * @brief
     */
    class FakeWorkingMemoryObjectLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        FakeWorkingMemoryObjectLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the Working Memory to be used");
        }
    };

    /**
     * @defgroup Component-FakeWorkingMemoryObjectLocalizer FakeWorkingMemoryObjectLocalizer
     * @ingroup ArmarXSimulation-Components
     * A description of the component FakeWorkingMemoryObjectLocalizer.
     *
     * @class FakeWorkingMemoryObjectLocalizer
     * @ingroup Component-FakeWorkingMemoryObjectLocalizer
     * @brief Brief description of class FakeWorkingMemoryObjectLocalizer.
     *
     * Detailed description of class FakeWorkingMemoryObjectLocalizer.
     */
    class FakeWorkingMemoryObjectLocalizer :
        virtual public armarx::Component,
        virtual public memoryx::ObjectLocalizerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ObjectLocalizerInterface interface
    public:
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames, const Ice::Current&) override;

    protected:
        memoryx::WorkingMemoryInterfacePrx wm;
    };
}

