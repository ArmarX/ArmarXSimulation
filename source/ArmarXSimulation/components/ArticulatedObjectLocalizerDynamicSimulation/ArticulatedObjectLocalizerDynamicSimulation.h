/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::ArticulatedObjectLocalizerDynamicSimulation
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <unordered_map>
#include <vector>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/util/tasks.h>

#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem_objects/types.h>
#include <RobotAPI/libraries/armem_objects/client/articulated_object/Writer.h>
#include <RobotAPI/libraries/armem_objects/client/articulated_object/Reader.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>


namespace armarx
{

    /**
     * @defgroup Component-ArticulatedObjectLocalizerDynamicSimulation ArticulatedObjectLocalizerDynamicSimulation
     * @ingroup ArmarXSimulation-Components
     * A description of the component ArticulatedObjectLocalizerDynamicSimulation.
     *
     * @class ArticulatedObjectLocalizerDynamicSimulation
     * @ingroup Component-ArticulatedObjectLocalizerDynamicSimulation
     * @brief Brief description of class ArticulatedObjectLocalizerDynamicSimulation.
     *
     * Detailed description of class ArticulatedObjectLocalizerDynamicSimulation.
     */
    class ArticulatedObjectLocalizerDynamicSimulation :
        virtual public armarx::Component,
        virtual public armarx::armem::client::ListeningPluginUser
    {
    public:

        ArticulatedObjectLocalizerDynamicSimulation();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        void setupObjects();
        void localizeObjects();

    private:

        // Private member variables go here.
        SimulatorInterfacePrx simulator;

        std::unique_ptr<::armarx::armem::articulated_object::Writer> articulatedObjectWriter;
        std::unique_ptr<::armarx::armem::articulated_object::Reader> articulatedObjectReader;

        PeriodicTask<ArticulatedObjectLocalizerDynamicSimulation>::pointer_type task;
        PeriodicTask<ArticulatedObjectLocalizerDynamicSimulation>::pointer_type taskUpdateObjects;

        std::mutex knownObjectsMtx;
        std::unordered_map<std::string, std::shared_ptr<VirtualRobot::Robot>> knownObjects;
        
        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            int cycleTime = 30;
            int cycleTimeUpdateObjectsInSimulator = 500;
            std::vector<std::string> objects = {};
        };
        Properties p;

    };
}
