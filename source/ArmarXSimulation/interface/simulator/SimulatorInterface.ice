/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Simulator
 * @author     Nikolaus Vahrenkamp
 * @copyright  2015 Nikolaus Vahrenkamp
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>

#include <ArmarXCore/interface/core/PackagePath.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/TimeServerInterface.ice>

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>
#include <RobotAPI/interface/units/HandUnitInterface.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/units/RobotPoseUnitInterface.ice>
#include <RobotAPI/interface/units/ForceTorqueUnit.ice>
#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>

module armarx
{
    struct ForceTorqueData
    {
        Vector3Base force;
        Vector3Base torque;
        string sensorName;
        string nodeName;
    };
    sequence<ForceTorqueData> ForceTorqueDataSeq;

    dictionary< string, PoseBase > NamePoseMap;

    struct ContactInformation
    {
        string objectNameA;
        string objectNameB;
        Vector3Base posGlobalA;
        Vector3Base posGlobalB;
    };

    sequence< ContactInformation > ContactInfoSequence;

    struct GraspingInformation
    {
        string robotNode;
        string objectName;
        PoseBase node2objectTransformation;
    };

    sequence< GraspingInformation > GraspInfoSequence;

    struct RobotVisuData
    {
        string name;
        PoseBase pose;
        NamePoseMap robotNodePoses;

        NameValueMap jointValues;

        GraspInfoSequence attachedObjects;

        bool updated;

        string robotFile;
        string project;

        float scaling;

        bool colModel;
    };

    struct SimulatedRobotState
    {
        bool hasRobot = false;
        long timestampInMicroSeconds = 0;

        NameValueMap jointAngles;
        NameValueMap jointVelocities;
        NameValueMap jointTorques;

        ForceTorqueDataSeq forceTorqueValues;

        PoseBase pose;
        Vector3Base linearVelocity;
        Vector3Base angularVelocity;
    };

    enum ObjectType {Box,Sphere,Cylinder};

    class ObjectVisuPrimitive
    {
        ObjectType type;
        DrawColor color;
        float massKG;
    };

    class BoxVisuPrimitive extends ObjectVisuPrimitive
    {
        float width;
        float height;
        float depth;
    };
    class SphereVisuPrimitive extends ObjectVisuPrimitive
    {
        float radius;
    };
    class CylinderVisuPrimitive extends ObjectVisuPrimitive
    {
        float radius;
        float length;
    };

    struct ObjectVisuData
    {
        string name;
        NamePoseMap objectPoses; // there might be children which should also be updated... (currently not used)
        bool updated;

        bool staticObject;

        // either the object was generated from a file
        string filename;
        string project;

        // or the object was created from an objectClass
        string objectClassName;

        // or its a primitive
        ObjectVisuPrimitive objectPrimitiveData;
    };

    sequence<RobotVisuData> RobotVisuList;
    sequence<ObjectVisuData> ObjectVisuList;
    //sequence<ContactVisuData> ContactVisuList;
    struct SceneVisuData
    {
        RobotVisuList robots;
        ObjectVisuList objects;

        bool floor;
        string floorTextureFile;

        // if data comes from a longterm memory snapshot, the interface names are stored here
        string priorKnowledgeName;
        string commonStorageName;
        long timestamp;
    };



    struct SimulatorInformation
    {
        /**
         * @brief simTimeStepMeasuredMS The actual needed simulation time in ms (to simulate one step).
         */
        float simTimeStepMeasuredMS;

        /**
         * @brief simTimeStepDurationMS The requested simulation time step in ms.
         */
        float simTimeStepDurationMS;

        /*!
         * \brief simTimeFactor The average factor between requested and actual simulation.
         */
        float simTimeFactor;

        float comTimeMS;
        float syncEngineTimeMS;

        int nrRobots;
        int nrActuatedJoints;
        int nrObjects;
        int nrContacts;

        float currentSimulatorTimeSec;

        RobotVisuList robots;
        ObjectVisuList objects;
    };



    /*!
      */
    struct ObjectClassInformation
    {
        string className;
        string manipulationObjectName;
        FramedPoseBase pose;
        long timestampMicroSeconds;
    };

    sequence<ObjectClassInformation> ObjectClassInformationSequence;

    struct DistanceInfo
    {
        Vector3Base p1;
        Vector3Base p2;
        float distance;
    };

    /*!
     * \brief The SimulationType enum Kinematic objects can be moved manually, but do not underly gravity.
     */
    enum SimulationType {Kinematic, Dynamic};


    interface SimulatorInterface extends TimeServerInterface
    {
        void actuateRobotJoints(string robotName, NameValueMap angles, NameValueMap velocities);
        void actuateRobotJointsVel(string robotName, NameValueMap velocities);
        void actuateRobotJointsPos(string robotName, NameValueMap angles);
        void actuateRobotJointsTorque(string robotName, NameValueMap torques);
        void setRobotPose(string robotName, PoseBase globalPose);

        void applyForceRobotNode(string robotName, string robotNodeName, Vector3Base force);
        void applyTorqueRobotNode(string robotName, string robotNodeName, Vector3Base torque);

        void applyForceObject(string objectName, Vector3Base force);
        void applyTorqueObject(string objectName, Vector3Base torque);

        // addRobotFromFile(armarx::data::PackagePath) should be preferred
        string addRobot(string filename);
        string addScaledRobot(string filename, float scale);
        string addScaledRobotName(string instanceName, string filename, float scale);
        bool removeRobot(string robotName);
        void addObject(memoryx::ObjectClassBase objectClassBase, string instanceName, PoseBase globalPose, bool isStatic);
       
        string addRobotFromFile(armarx::data::PackagePath packagePath);
        void addObjectFromFile(armarx::data::PackagePath packagePath, string instanceName, PoseBase globalPose, bool isStatic);
        
        void addBox(float width, float height, float depth, float massKG, DrawColor color, string instanceName, PoseBase globalPose, bool isStatic);

        void removeObject(string instanceName);

        bool hasObject(string instanceName);
        void setObjectPose(string objectName, PoseBase globalPose);

        /*!
         * \brief setObjectSimulationType Kinematic objects can be moved manually, but do not underly gravity.
         * \param objectName
         * \param type
         */
        void setObjectSimulationType(string objectName, SimulationType type);

        /*!
         * \brief activateObject Sleeping objects can be activated manually. Usually object activation is handled by the physics engine, but only for Dynamic objects.
         * \param objectName
         */
        void activateObject(string objectName);
        Ice::StringSeq getRobotNames();

        bool hasRobot(string robotName);
        bool hasRobotNode(string robotName, string robotNodeName);
        float getRobotMass(string robotName);
        NameValueMap getRobotJointAngles(string robotName);
        float getRobotJointAngle(string robotName, string nodeName);
        NameValueMap getRobotJointVelocities(string robotName);
        NameValueMap getRobotJointTorques(string robotName);
        ForceTorqueDataSeq getRobotForceTorqueSensors(string robotName);

        float getRobotJointVelocity(string robot, string nodeName);
        float getRobotJointLimitLo(string robot, string nodeName);
        float getRobotJointLimitHi(string robot, string nodeName);

        float getRobotMaxTorque(string robotName, string nodeName);
        void setRobotMaxTorque(string robotName, string nodeName, float maxTorque);

        SimulatedRobotState getRobotState(string robotName);
        PoseBase getRobotPose(string robotName);
        PoseBase getRobotNodePose(string robotName, string robotNodeName);
        Vector3Base getRobotLinearVelocity(string robotName, string nodeName);
        Vector3Base getRobotAngularVelocity(string robotName, string nodeName);

        void setRobotLinearVelocity(string robotName, string nodeName, Vector3Base vel);
        void setRobotAngularVelocity(string robotName, string nodeName, Vector3Base vel);
        void setRobotLinearVelocityRobotRootFrame(string robotName, string nodeName, Vector3Base vel);
        void setRobotAngularVelocityRobotRootFrame(string robotName, string nodeName, Vector3Base vel);


        //! Inform the simulator that an object has been successfully grasped.
        void objectGrasped(string robotName, string robotNodeName, string objectName);
        //! Inform the simulator that an object has been released.
        void objectReleased(string robotName, string robotNodeName, string objectName);

        /*!
          This method can be used to query for simox objects in the world.
          */
        PoseBase getObjectPose(string objectName);

        DistanceInfo getDistance(string robotName, string robotNodeName, string worldObjectName);
        DistanceInfo getRobotNodeDistance(string robotName, string robotNodeName1, string robotNodeName2);

        /*!
          * Enable/Disable visualization of contacts. The contacts are shown via the Debug/Entity Drawer.
          * The string layerName is used to identify the layer on which the contacts are drawn. By default this should be "contacts".
        */
        void showContacts(bool enable, string layerName);

        /*!
         * \brief Returns a list of all contacts. Note that you must call updateContacts() first to enable
         * contacts handling.
         */
        ContactInfoSequence getContacts();

        /*!
         * \brief Enables the handling of contacts. If you intend to use getContacts(), call this method
         * with enabled = true first.
         */
        void updateContacts(bool enable);

        /*!
          This method can be used to query for a class of objects in the world.
          The className is related to the classname of the snapshot that was used for loading the scene.
          If no snapshot was used, the simulator will query all simox ManipulationObjects to search one with name==className
        */
        ObjectClassInformationSequence getObjectClassPoses(string robotName, string frameName, string className);

        SceneVisuData getScene();

        int getFixedTimeStepMS();

        float getSimTime();

        SimulatorInformation getSimulatorInformation();

        //void start(); // also in TimeServerInterface
        void pause();
        //void step(); // also in TimeServerInterface

        bool isRunning();


        /*!
         * \brief reInitialize Do a re-initialization of the simulation environment (i.e. setup the scene as if you would restart the simulator)
         * This re-loads the robot, clears the working memory and re-loads the scene as defined via the simulator config (either from an xml file or by re-loading the memroyx snapshot)
         */
        void reInitialize();
    };



    interface SimulatorRobotListenerInterface
    {
        void reportState(SimulatedRobotState state);
    };

    interface SimulatorForceTorqueListenerInterface
    {
        /*!
            Reports force/torque values together with the robot's reference frame.
        */
        void reportForceTorque(Vector3Base force, Vector3Base torque, string sensorName, string nodeName, bool aValueChanged);
    };

    interface SimulatorObjectListener
    {
        void reportPose(PoseBase actualPose, bool aValueChanged);
    };


    struct SimulatorViewerInformation
    {
        float drawTimeMS;
        float comTimeMS;
        bool windowEnabled;
    };

    interface SimulatorListenerInterface
    {
        void reportSceneUpdated(SceneVisuData scene);
    };

    interface SimulatorViewerControlInterface
    {
        void enableSimulatorWindow(bool show);

        //! Switch between full/collision model
        void selectVisuType(bool fullModel);

        void showBaseCoordSystem(bool show, float scale);

        SimulatorViewerInformation getSimulatorInformation();

        void showDebugDrawLayer(bool show);

        //void showContacts(bool show);

        void setTempPath(string p);
        void saveScreenshots(bool enable);

        void clearDebugDrawLayer();

        void setAntiAliasing(int steps);

        PoseBase getCameraPose();

        /*!
         * \brief Returns information about each layer.
         * \return Information about each layer.
         */
        LayerInformationSequence layerInformation();

        /*!
         * \brief clear removes all visualizations for the given layer
         * \param layerName The name identifies the layer.
         */
        void clearLayer(string layerName);

        /*!
         * \brief Sets the layer's visibility.
         * \param layerName The layer.
         * \param visible Whether the layer will be visible.

         */

        void enableLayerVisu(string layerName, bool visible);

        /*!
         * \brief Removes a layer and all of its content.
         * \param layerName The layer.
         */
        void removeLayer(string layerName);
    };

    interface KinematicUnitDynamicSimulationInterface extends KinematicUnitInterface, SimulatorRobotListenerInterface
    {
    };

    interface PlatformUnitDynamicSimulationInterface extends PlatformUnitInterface, SimulatorRobotListenerInterface
    {
    };

    interface RobotPoseUnitDynamicSimulationInterface extends RobotPoseUnitInterface, SimulatorRobotListenerInterface
    {
    };

    interface ForceTorqueUnitDynamicSimulationInterface extends ForceTorqueUnitInterface, SimulatorForceTorqueListenerInterface
    {
    };
};
