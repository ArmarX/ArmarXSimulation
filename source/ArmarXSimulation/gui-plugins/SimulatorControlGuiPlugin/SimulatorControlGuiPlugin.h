/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::MemoryX::WorkingMemoryGuiPlugin
* @author     Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/** ArmarX headers **/
#include <ArmarXSimulation/gui-plugins/SimulatorControlGuiPlugin/ui_SimulatorControlGuiPlugin.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/Pose.h>

/** Qt headers **/
#include <QMainWindow>
#include <QString>
#include <QSignalMapper>
#include <QTimer>
#include <QToolBar>
#include <QTableWidgetItem>
#include <QCheckBox>

/** Coin3D/SoQt headers **/
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/Qt/SoQt.h>

/** VirtualRobot headers **/
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

/** STD headers **/
#include <unordered_map>
#include <memory>

namespace armarx
{
    /**
     * \class SimulatorControlGuiPlugin
     * \brief This plugin offers a widget to control a simulation.
     * \see SimulatorControlController
     * \ingroup ArmarXSimulatorComponents
     */

    class SimulatorControlGuiPlugin :
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        SimulatorControlGuiPlugin();
        QString getPluginName() override
        {
            return "SimulatorControlGuiPlugin";
        }
    };

    /**
      \page ArmarXSimulation-GuiPlugins-SimulatorControl SimulatorControl Gui

      \brief A widget to control a simulation.

      This widget provides information and allows controlling the simulation.

      \image html SimulatorControlGuiPlugin_Widget_800.png "The plugin's ui." width=300px
           -# A table containing all visualization layers. The layers can be turned off and on, cleared and removed.
           -# Whether the simulation window is visible.
           -# Whether a coordinate system at (0,0,0) is shown.
           -# Whether contacts are shown. @image html SimulatorControlGuiPlugin_Collision_mod.png "Left: The full model. Right: The collision model" width=300px
           -# Whether the full model or collision model is rendered. @image html SimulatorControlGuiPlugin_Contacts.png "Left: Contacts visible. Right: Contacts invisible" width=300px
           -# The level of anti aliasing
           -# Some information about the simulation. (timings, number of robots, joints, objects and contacts, camera position and orientation)

      SimulatorControl API Documenation \ref SimulatorControlController
      \ingroup ArmarXSimulatorComponents
      \see SimulatorControlGuiPlugin
     */

    /**
     * \brief A widget to control a simulation.
     *
     * This widget provides information and allows controlling the simulation.
     * \ingroup ArmarXSimulatorComponents
     * \see SimulatorControlGuiPlugin
     */
    class SimulatorControlController :
        public ArmarXComponentWidgetControllerTemplate<SimulatorControlController>
    {
        Q_OBJECT
    public:
        SimulatorControlController();
        virtual ~SimulatorControlController() {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "SimulatorControlGuiPlugin";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/monitor_and_remote.svg"};
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;

    signals:
        void robotsChanged();

    public slots:
        void showSimWindow(bool enable);
        void showCoordSystem(bool enable);
        void showContacts(bool enable);
        void selectVisuType();
        void playStopSim();
        void stepSim();
        /*!
         * \brief reInit Re-loads all simulator content.
         */
        void reInit();
        void antiAliasing(int steps);

        /**
         * @brief Removes a layer.
         * @param layerName The layer to remove.
         */
        void layerRemove(QString layerName);
        /**
         * @brief Toggles a layer's visibility
         * @param layerName The layer.
         */
        void layerToggleVisibility(QString layerName);
        /**
         * @brief Requests the current layer information from simulatorPrx and updates the table.
         */
        void layerPoll();
        /**
         * @brief Clears a layer.
         * @param layerName The layer to clear.
         */
        void layerClear(QString layerName);

    protected:
        /**
         * @brief timerEvent
         * @param event
         */
        void timerEvent(QTimerEvent*) override;

        void connectSlots();

        Ui::SimulatorControlGuiPlugin ui;

        bool verbose;

        SimulatorInterfacePrx simulatorPrx;
        SimulatorViewerControlInterfacePrx simulatorViewerPrx;

    private slots:
        void on_pushButtonManipRobSetPose_clicked();

    private slots:
        void on_pushButtonManipRobotUpdate_clicked();

    private slots:
        void on_pushButtonAddRobot_clicked();

    private:
        std::string settings_simulatorPrxName;
        std::string settings_simulatorViewerPrxName;

        /**
         * @brief Triggers the request for layer information
         */
        QTimer layerPollTimer;
        /**
         * @brief Maps events to toggle a layer's visibility from checkboxes contained in the table to layerToggleVisibility.
         */
        QSignalMapper layerSignalMapperVisible;
        /**
         * @brief Maps events to clear a layer from corresponding buttons contained in the table to layerClear
         */
        QSignalMapper layerSignalMapperClear;
        /**
         * @brief Maps events to remove a layer from corresponding buttons contained in the table to layerClear
         */
        QSignalMapper layerSignalMapperRemove;

        using LayerVisibilityCheckBoxesType = std::map<std::string, std::pair<QTableWidgetItem*, QPointer<QCheckBox>>>;
        /**
         * @brief Stores checkboxes to turn layers visible / invisible.
         */
        LayerVisibilityCheckBoxesType layerVisibilityCheckBoxes;

        int timerId;

        QPointer<QToolBar> customToolbar;
    };
    using SimulatorControlGuiPluginPtr = std::shared_ptr<SimulatorControlGuiPlugin>;
}



