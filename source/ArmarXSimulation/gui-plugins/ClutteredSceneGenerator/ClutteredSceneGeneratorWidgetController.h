/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::gui-plugins::ClutteredSceneGeneratorWidgetController
 * @author     Patrick Hegemann ( patrick dot hegemann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXSimulation/libraries/ClutteredSceneGenerator/ClutteredSceneGenerator.h>
#include <ArmarXSimulation/gui-plugins/ClutteredSceneGenerator/ui_ClutteredSceneGeneratorWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include "ArmarXCore/core/PackagePath.h"
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <QListWidgetItem>

#include <filesystem>

namespace armarx
{
    /**
    \page ArmarXSimulation-GuiPlugins-ClutteredSceneGenerator ClutteredSceneGenerator
    \brief The ClutteredSceneGenerator allows visualizing ...

    \image html ClutteredSceneGenerator.png
    The user can

    API Documentation \ref ClutteredSceneGeneratorWidgetController

    \see ClutteredSceneGeneratorGuiPlugin
    */

    /**
     * \class ClutteredSceneGeneratorGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief ClutteredSceneGeneratorGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class ClutteredSceneGeneratorWidgetController
     * \brief ClutteredSceneGeneratorWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ClutteredSceneGeneratorWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < ClutteredSceneGeneratorWidgetController >
    {
        Q_OBJECT

    public:
        /// Controller Constructor
        explicit ClutteredSceneGeneratorWidgetController();
        /// Controller destructor
        virtual ~ClutteredSceneGeneratorWidgetController();

        //// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;
        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "ClutteredSceneGenerator";
        }

        /// \see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// \see armarx::Component::onConnectComponent()
        void onConnectComponent() override;

    public slots:
        /* QT slot declarations */
        // Objects tab
        void generateObjects();

        // Scene generation tab
        void refreshSets();
        void updateObjectPoseConstraints();
        void generateScene();

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::ClutteredSceneGeneratorWidget widget_{};

        // Items in the list of object sets
        std::vector<QListWidgetItem*> objectSetListItems_;

        // (Logic) object sets
        std::map<std::string, simulation::scene_generation::ObjectSet> objectSets_;
        
        // Generate the configuration for the cluttered scene generator
        simulation::scene_generation::ClutteredSceneGenerator::Config getGenConfig();

        // Simulator proxy
        std::string simulatorProxyName_ = "Simulator";
        SimulatorInterfacePrx simulator_;

        std::map<std::string, simulation::scene_generation::ObjectSet>
        getObjectSets(const armarx::PackagePath& packagePath);
    };
}  // namespace armarx
